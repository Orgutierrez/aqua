/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AquaAppTestModule } from '../../../test.module';
import { HistoricoMedidorComponent } from 'app/entities/historico-medidor/historico-medidor.component';
import { HistoricoMedidorService } from 'app/entities/historico-medidor/historico-medidor.service';
import { HistoricoMedidor } from 'app/shared/model/historico-medidor.model';

describe('Component Tests', () => {
  describe('HistoricoMedidor Management Component', () => {
    let comp: HistoricoMedidorComponent;
    let fixture: ComponentFixture<HistoricoMedidorComponent>;
    let service: HistoricoMedidorService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AquaAppTestModule],
        declarations: [HistoricoMedidorComponent],
        providers: []
      })
        .overrideTemplate(HistoricoMedidorComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(HistoricoMedidorComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HistoricoMedidorService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new HistoricoMedidor(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.historicoMedidors[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
