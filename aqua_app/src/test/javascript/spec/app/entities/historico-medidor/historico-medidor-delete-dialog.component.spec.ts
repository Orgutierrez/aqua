/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AquaAppTestModule } from '../../../test.module';
import { HistoricoMedidorDeleteDialogComponent } from 'app/entities/historico-medidor/historico-medidor-delete-dialog.component';
import { HistoricoMedidorService } from 'app/entities/historico-medidor/historico-medidor.service';

describe('Component Tests', () => {
  describe('HistoricoMedidor Management Delete Component', () => {
    let comp: HistoricoMedidorDeleteDialogComponent;
    let fixture: ComponentFixture<HistoricoMedidorDeleteDialogComponent>;
    let service: HistoricoMedidorService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AquaAppTestModule],
        declarations: [HistoricoMedidorDeleteDialogComponent]
      })
        .overrideTemplate(HistoricoMedidorDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(HistoricoMedidorDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HistoricoMedidorService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
