/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AquaAppTestModule } from '../../../test.module';
import { HistoricoMedidorDetailComponent } from 'app/entities/historico-medidor/historico-medidor-detail.component';
import { HistoricoMedidor } from 'app/shared/model/historico-medidor.model';

describe('Component Tests', () => {
  describe('HistoricoMedidor Management Detail Component', () => {
    let comp: HistoricoMedidorDetailComponent;
    let fixture: ComponentFixture<HistoricoMedidorDetailComponent>;
    const route = ({ data: of({ historicoMedidor: new HistoricoMedidor(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AquaAppTestModule],
        declarations: [HistoricoMedidorDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(HistoricoMedidorDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(HistoricoMedidorDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.historicoMedidor).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
