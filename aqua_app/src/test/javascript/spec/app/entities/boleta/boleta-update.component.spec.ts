/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { AquaAppTestModule } from '../../../test.module';
import { BoletaUpdateComponent } from 'app/entities/boleta/boleta-update.component';
import { BoletaService } from 'app/entities/boleta/boleta.service';
import { Boleta } from 'app/shared/model/boleta.model';

describe('Component Tests', () => {
  describe('Boleta Management Update Component', () => {
    let comp: BoletaUpdateComponent;
    let fixture: ComponentFixture<BoletaUpdateComponent>;
    let service: BoletaService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AquaAppTestModule],
        declarations: [BoletaUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BoletaUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BoletaUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BoletaService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Boleta(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Boleta();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
