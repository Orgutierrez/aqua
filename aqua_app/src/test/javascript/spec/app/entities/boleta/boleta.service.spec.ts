/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { BoletaService } from 'app/entities/boleta/boleta.service';
import { IBoleta, Boleta } from 'app/shared/model/boleta.model';

describe('Service Tests', () => {
  describe('Boleta Service', () => {
    let injector: TestBed;
    let service: BoletaService;
    let httpMock: HttpTestingController;
    let elemDefault: IBoleta;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(BoletaService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Boleta(
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        'AAAAAAA',
        'AAAAAAA',
        0,
        'AAAAAAA',
        0,
        currentDate,
        currentDate,
        currentDate,
        currentDate
      );
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            fechaVencimiento: currentDate.format(DATE_TIME_FORMAT),
            fechaEmision: currentDate.format(DATE_TIME_FORMAT),
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            lastUpdDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Boleta', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            fechaVencimiento: currentDate.format(DATE_TIME_FORMAT),
            fechaEmision: currentDate.format(DATE_TIME_FORMAT),
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            lastUpdDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            fechaVencimiento: currentDate,
            fechaEmision: currentDate,
            createdDate: currentDate,
            lastUpdDate: currentDate
          },
          returnedFromService
        );
        service
          .create(new Boleta(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Boleta', async () => {
        const returnedFromService = Object.assign(
          {
            numeroBoleta: 1,
            consumoCalculado: 1,
            consumoFacturado: 1,
            cargoFijo: 1,
            consumoAguaPotable: 1,
            servAlcantarillado: 1,
            tratAguasServidas: 1,
            subConsumoMes: 1,
            despachoPostalCer: 1,
            intereses: 1,
            ajusteSencilloAnte: 1,
            ajusteSencillo: 1,
            montoTotal: 1,
            saldoAnterior: 1,
            totalPagar: 1,
            tipoDespacho: 'BBBBBB',
            grupoTarifario: 'BBBBBB',
            factorCobro: 1,
            tipoFactura: 'BBBBBB',
            diametro: 1,
            fechaVencimiento: currentDate.format(DATE_TIME_FORMAT),
            fechaEmision: currentDate.format(DATE_TIME_FORMAT),
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            lastUpdDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            fechaVencimiento: currentDate,
            fechaEmision: currentDate,
            createdDate: currentDate,
            lastUpdDate: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Boleta', async () => {
        const returnedFromService = Object.assign(
          {
            numeroBoleta: 1,
            consumoCalculado: 1,
            consumoFacturado: 1,
            cargoFijo: 1,
            consumoAguaPotable: 1,
            servAlcantarillado: 1,
            tratAguasServidas: 1,
            subConsumoMes: 1,
            despachoPostalCer: 1,
            intereses: 1,
            ajusteSencilloAnte: 1,
            ajusteSencillo: 1,
            montoTotal: 1,
            saldoAnterior: 1,
            totalPagar: 1,
            tipoDespacho: 'BBBBBB',
            grupoTarifario: 'BBBBBB',
            factorCobro: 1,
            tipoFactura: 'BBBBBB',
            diametro: 1,
            fechaVencimiento: currentDate.format(DATE_TIME_FORMAT),
            fechaEmision: currentDate.format(DATE_TIME_FORMAT),
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            lastUpdDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            fechaVencimiento: currentDate,
            fechaEmision: currentDate,
            createdDate: currentDate,
            lastUpdDate: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Boleta', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
