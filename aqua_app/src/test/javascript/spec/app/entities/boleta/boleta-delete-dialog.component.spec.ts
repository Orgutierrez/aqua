/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AquaAppTestModule } from '../../../test.module';
import { BoletaDeleteDialogComponent } from 'app/entities/boleta/boleta-delete-dialog.component';
import { BoletaService } from 'app/entities/boleta/boleta.service';

describe('Component Tests', () => {
  describe('Boleta Management Delete Component', () => {
    let comp: BoletaDeleteDialogComponent;
    let fixture: ComponentFixture<BoletaDeleteDialogComponent>;
    let service: BoletaService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AquaAppTestModule],
        declarations: [BoletaDeleteDialogComponent]
      })
        .overrideTemplate(BoletaDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BoletaDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BoletaService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
