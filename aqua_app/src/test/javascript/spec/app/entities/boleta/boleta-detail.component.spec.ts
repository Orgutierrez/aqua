/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AquaAppTestModule } from '../../../test.module';
import { BoletaDetailComponent } from 'app/entities/boleta/boleta-detail.component';
import { Boleta } from 'app/shared/model/boleta.model';

describe('Component Tests', () => {
  describe('Boleta Management Detail Component', () => {
    let comp: BoletaDetailComponent;
    let fixture: ComponentFixture<BoletaDetailComponent>;
    const route = ({ data: of({ boleta: new Boleta(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AquaAppTestModule],
        declarations: [BoletaDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BoletaDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BoletaDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.boleta).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
