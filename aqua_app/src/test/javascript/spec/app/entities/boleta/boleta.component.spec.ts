/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AquaAppTestModule } from '../../../test.module';
import { BoletaComponent } from 'app/entities/boleta/boleta.component';
import { BoletaService } from 'app/entities/boleta/boleta.service';
import { Boleta } from 'app/shared/model/boleta.model';

describe('Component Tests', () => {
  describe('Boleta Management Component', () => {
    let comp: BoletaComponent;
    let fixture: ComponentFixture<BoletaComponent>;
    let service: BoletaService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AquaAppTestModule],
        declarations: [BoletaComponent],
        providers: []
      })
        .overrideTemplate(BoletaComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BoletaComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BoletaService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Boleta(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.boletas[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
