/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AquaAppTestModule } from '../../../test.module';
import { MedidorComponent } from 'app/entities/medidor/medidor.component';
import { MedidorService } from 'app/entities/medidor/medidor.service';
import { Medidor } from 'app/shared/model/medidor.model';

describe('Component Tests', () => {
  describe('Medidor Management Component', () => {
    let comp: MedidorComponent;
    let fixture: ComponentFixture<MedidorComponent>;
    let service: MedidorService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AquaAppTestModule],
        declarations: [MedidorComponent],
        providers: []
      })
        .overrideTemplate(MedidorComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MedidorComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MedidorService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Medidor(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.medidors[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
