/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AquaAppTestModule } from '../../../test.module';
import { MedidorDetailComponent } from 'app/entities/medidor/medidor-detail.component';
import { Medidor } from 'app/shared/model/medidor.model';

describe('Component Tests', () => {
  describe('Medidor Management Detail Component', () => {
    let comp: MedidorDetailComponent;
    let fixture: ComponentFixture<MedidorDetailComponent>;
    const route = ({ data: of({ medidor: new Medidor(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AquaAppTestModule],
        declarations: [MedidorDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(MedidorDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MedidorDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.medidor).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
