/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { AquaAppTestModule } from '../../../test.module';
import { MedidorUpdateComponent } from 'app/entities/medidor/medidor-update.component';
import { MedidorService } from 'app/entities/medidor/medidor.service';
import { Medidor } from 'app/shared/model/medidor.model';

describe('Component Tests', () => {
  describe('Medidor Management Update Component', () => {
    let comp: MedidorUpdateComponent;
    let fixture: ComponentFixture<MedidorUpdateComponent>;
    let service: MedidorService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AquaAppTestModule],
        declarations: [MedidorUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(MedidorUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MedidorUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MedidorService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Medidor(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Medidor();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
