package com.nior.aqua.web.rest;

import com.nior.aqua.AquaAppApp;
import com.nior.aqua.domain.Boleta;
import com.nior.aqua.domain.Medidor;
import com.nior.aqua.repository.BoletaRepository;
import com.nior.aqua.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.nior.aqua.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BoletaResource} REST controller.
 */
@SpringBootTest(classes = AquaAppApp.class)
public class BoletaResourceIT {

    private static final Long DEFAULT_NUMERO_BOLETA = 1L;
    private static final Long UPDATED_NUMERO_BOLETA = 2L;
    private static final Long SMALLER_NUMERO_BOLETA = 1L - 1L;

    private static final Integer DEFAULT_CONSUMO_CALCULADO = 1;
    private static final Integer UPDATED_CONSUMO_CALCULADO = 2;
    private static final Integer SMALLER_CONSUMO_CALCULADO = 1 - 1;

    private static final Integer DEFAULT_CONSUMO_FACTURADO = 1;
    private static final Integer UPDATED_CONSUMO_FACTURADO = 2;
    private static final Integer SMALLER_CONSUMO_FACTURADO = 1 - 1;

    private static final Integer DEFAULT_CARGO_FIJO = 1;
    private static final Integer UPDATED_CARGO_FIJO = 2;
    private static final Integer SMALLER_CARGO_FIJO = 1 - 1;

    private static final Long DEFAULT_CONSUMO_AGUA_POTABLE = 1L;
    private static final Long UPDATED_CONSUMO_AGUA_POTABLE = 2L;
    private static final Long SMALLER_CONSUMO_AGUA_POTABLE = 1L - 1L;

    private static final Long DEFAULT_SERV_ALCANTARILLADO = 1L;
    private static final Long UPDATED_SERV_ALCANTARILLADO = 2L;
    private static final Long SMALLER_SERV_ALCANTARILLADO = 1L - 1L;

    private static final Long DEFAULT_TRAT_AGUAS_SERVIDAS = 1L;
    private static final Long UPDATED_TRAT_AGUAS_SERVIDAS = 2L;
    private static final Long SMALLER_TRAT_AGUAS_SERVIDAS = 1L - 1L;

    private static final Long DEFAULT_SUB_CONSUMO_MES = 1L;
    private static final Long UPDATED_SUB_CONSUMO_MES = 2L;
    private static final Long SMALLER_SUB_CONSUMO_MES = 1L - 1L;

    private static final Long DEFAULT_DESPACHO_POSTAL_CER = 1L;
    private static final Long UPDATED_DESPACHO_POSTAL_CER = 2L;
    private static final Long SMALLER_DESPACHO_POSTAL_CER = 1L - 1L;

    private static final Long DEFAULT_INTERESES = 1L;
    private static final Long UPDATED_INTERESES = 2L;
    private static final Long SMALLER_INTERESES = 1L - 1L;

    private static final Long DEFAULT_AJUSTE_SENCILLO_ANTE = 1L;
    private static final Long UPDATED_AJUSTE_SENCILLO_ANTE = 2L;
    private static final Long SMALLER_AJUSTE_SENCILLO_ANTE = 1L - 1L;

    private static final Long DEFAULT_AJUSTE_SENCILLO = 1L;
    private static final Long UPDATED_AJUSTE_SENCILLO = 2L;
    private static final Long SMALLER_AJUSTE_SENCILLO = 1L - 1L;

    private static final Long DEFAULT_MONTO_TOTAL = 1L;
    private static final Long UPDATED_MONTO_TOTAL = 2L;
    private static final Long SMALLER_MONTO_TOTAL = 1L - 1L;

    private static final Long DEFAULT_SALDO_ANTERIOR = 1L;
    private static final Long UPDATED_SALDO_ANTERIOR = 2L;
    private static final Long SMALLER_SALDO_ANTERIOR = 1L - 1L;

    private static final Long DEFAULT_TOTAL_PAGAR = 1L;
    private static final Long UPDATED_TOTAL_PAGAR = 2L;
    private static final Long SMALLER_TOTAL_PAGAR = 1L - 1L;

    private static final String DEFAULT_TIPO_DESPACHO = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_DESPACHO = "BBBBBBBBBB";

    private static final String DEFAULT_GRUPO_TARIFARIO = "AAAAAAAAAA";
    private static final String UPDATED_GRUPO_TARIFARIO = "BBBBBBBBBB";

    private static final Float DEFAULT_FACTOR_COBRO = 1F;
    private static final Float UPDATED_FACTOR_COBRO = 2F;
    private static final Float SMALLER_FACTOR_COBRO = 1F - 1F;

    private static final String DEFAULT_TIPO_FACTURA = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_FACTURA = "BBBBBBBBBB";

    private static final Integer DEFAULT_DIAMETRO = 1;
    private static final Integer UPDATED_DIAMETRO = 2;
    private static final Integer SMALLER_DIAMETRO = 1 - 1;

    private static final Instant DEFAULT_FECHA_VENCIMIENTO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_VENCIMIENTO = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_FECHA_VENCIMIENTO = Instant.ofEpochMilli(-1L);

    private static final Instant DEFAULT_FECHA_EMISION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_EMISION = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_FECHA_EMISION = Instant.ofEpochMilli(-1L);

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_CREATED_DATE = Instant.ofEpochMilli(-1L);

    private static final Instant DEFAULT_LAST_UPD_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_UPD_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_LAST_UPD_DATE = Instant.ofEpochMilli(-1L);

    @Autowired
    private BoletaRepository boletaRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBoletaMockMvc;

    private Boleta boleta;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BoletaResource boletaResource = new BoletaResource(boletaRepository);
        this.restBoletaMockMvc = MockMvcBuilders.standaloneSetup(boletaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Boleta createEntity(EntityManager em) {
        Boleta boleta = new Boleta()
            .numeroBoleta(DEFAULT_NUMERO_BOLETA)
            .consumoCalculado(DEFAULT_CONSUMO_CALCULADO)
            .consumoFacturado(DEFAULT_CONSUMO_FACTURADO)
            .cargoFijo(DEFAULT_CARGO_FIJO)
            .consumoAguaPotable(DEFAULT_CONSUMO_AGUA_POTABLE)
            .servAlcantarillado(DEFAULT_SERV_ALCANTARILLADO)
            .tratAguasServidas(DEFAULT_TRAT_AGUAS_SERVIDAS)
            .subConsumoMes(DEFAULT_SUB_CONSUMO_MES)
            .despachoPostalCer(DEFAULT_DESPACHO_POSTAL_CER)
            .intereses(DEFAULT_INTERESES)
            .ajusteSencilloAnte(DEFAULT_AJUSTE_SENCILLO_ANTE)
            .ajusteSencillo(DEFAULT_AJUSTE_SENCILLO)
            .montoTotal(DEFAULT_MONTO_TOTAL)
            .saldoAnterior(DEFAULT_SALDO_ANTERIOR)
            .totalPagar(DEFAULT_TOTAL_PAGAR)
            .tipoDespacho(DEFAULT_TIPO_DESPACHO)
            .grupoTarifario(DEFAULT_GRUPO_TARIFARIO)
            .factorCobro(DEFAULT_FACTOR_COBRO)
            .tipoFactura(DEFAULT_TIPO_FACTURA)
            .diametro(DEFAULT_DIAMETRO)
            .fechaVencimiento(DEFAULT_FECHA_VENCIMIENTO)
            .fechaEmision(DEFAULT_FECHA_EMISION)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastUpdDate(DEFAULT_LAST_UPD_DATE);
        // Add required entity
        Medidor medidor;
        if (TestUtil.findAll(em, Medidor.class).isEmpty()) {
            medidor = MedidorResourceIT.createEntity(em);
            em.persist(medidor);
            em.flush();
        } else {
            medidor = TestUtil.findAll(em, Medidor.class).get(0);
        }
        boleta.setMedidor(medidor);
        return boleta;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Boleta createUpdatedEntity(EntityManager em) {
        Boleta boleta = new Boleta()
            .numeroBoleta(UPDATED_NUMERO_BOLETA)
            .consumoCalculado(UPDATED_CONSUMO_CALCULADO)
            .consumoFacturado(UPDATED_CONSUMO_FACTURADO)
            .cargoFijo(UPDATED_CARGO_FIJO)
            .consumoAguaPotable(UPDATED_CONSUMO_AGUA_POTABLE)
            .servAlcantarillado(UPDATED_SERV_ALCANTARILLADO)
            .tratAguasServidas(UPDATED_TRAT_AGUAS_SERVIDAS)
            .subConsumoMes(UPDATED_SUB_CONSUMO_MES)
            .despachoPostalCer(UPDATED_DESPACHO_POSTAL_CER)
            .intereses(UPDATED_INTERESES)
            .ajusteSencilloAnte(UPDATED_AJUSTE_SENCILLO_ANTE)
            .ajusteSencillo(UPDATED_AJUSTE_SENCILLO)
            .montoTotal(UPDATED_MONTO_TOTAL)
            .saldoAnterior(UPDATED_SALDO_ANTERIOR)
            .totalPagar(UPDATED_TOTAL_PAGAR)
            .tipoDespacho(UPDATED_TIPO_DESPACHO)
            .grupoTarifario(UPDATED_GRUPO_TARIFARIO)
            .factorCobro(UPDATED_FACTOR_COBRO)
            .tipoFactura(UPDATED_TIPO_FACTURA)
            .diametro(UPDATED_DIAMETRO)
            .fechaVencimiento(UPDATED_FECHA_VENCIMIENTO)
            .fechaEmision(UPDATED_FECHA_EMISION)
            .createdDate(UPDATED_CREATED_DATE)
            .lastUpdDate(UPDATED_LAST_UPD_DATE);
        // Add required entity
        Medidor medidor;
        if (TestUtil.findAll(em, Medidor.class).isEmpty()) {
            medidor = MedidorResourceIT.createUpdatedEntity(em);
            em.persist(medidor);
            em.flush();
        } else {
            medidor = TestUtil.findAll(em, Medidor.class).get(0);
        }
        boleta.setMedidor(medidor);
        return boleta;
    }

    @BeforeEach
    public void initTest() {
        boleta = createEntity(em);
    }

    @Test
    @Transactional
    public void createBoleta() throws Exception {
        int databaseSizeBeforeCreate = boletaRepository.findAll().size();

        // Create the Boleta
        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isCreated());

        // Validate the Boleta in the database
        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeCreate + 1);
        Boleta testBoleta = boletaList.get(boletaList.size() - 1);
        assertThat(testBoleta.getNumeroBoleta()).isEqualTo(DEFAULT_NUMERO_BOLETA);
        assertThat(testBoleta.getConsumoCalculado()).isEqualTo(DEFAULT_CONSUMO_CALCULADO);
        assertThat(testBoleta.getConsumoFacturado()).isEqualTo(DEFAULT_CONSUMO_FACTURADO);
        assertThat(testBoleta.getCargoFijo()).isEqualTo(DEFAULT_CARGO_FIJO);
        assertThat(testBoleta.getConsumoAguaPotable()).isEqualTo(DEFAULT_CONSUMO_AGUA_POTABLE);
        assertThat(testBoleta.getServAlcantarillado()).isEqualTo(DEFAULT_SERV_ALCANTARILLADO);
        assertThat(testBoleta.getTratAguasServidas()).isEqualTo(DEFAULT_TRAT_AGUAS_SERVIDAS);
        assertThat(testBoleta.getSubConsumoMes()).isEqualTo(DEFAULT_SUB_CONSUMO_MES);
        assertThat(testBoleta.getDespachoPostalCer()).isEqualTo(DEFAULT_DESPACHO_POSTAL_CER);
        assertThat(testBoleta.getIntereses()).isEqualTo(DEFAULT_INTERESES);
        assertThat(testBoleta.getAjusteSencilloAnte()).isEqualTo(DEFAULT_AJUSTE_SENCILLO_ANTE);
        assertThat(testBoleta.getAjusteSencillo()).isEqualTo(DEFAULT_AJUSTE_SENCILLO);
        assertThat(testBoleta.getMontoTotal()).isEqualTo(DEFAULT_MONTO_TOTAL);
        assertThat(testBoleta.getSaldoAnterior()).isEqualTo(DEFAULT_SALDO_ANTERIOR);
        assertThat(testBoleta.getTotalPagar()).isEqualTo(DEFAULT_TOTAL_PAGAR);
        assertThat(testBoleta.getTipoDespacho()).isEqualTo(DEFAULT_TIPO_DESPACHO);
        assertThat(testBoleta.getGrupoTarifario()).isEqualTo(DEFAULT_GRUPO_TARIFARIO);
        assertThat(testBoleta.getFactorCobro()).isEqualTo(DEFAULT_FACTOR_COBRO);
        assertThat(testBoleta.getTipoFactura()).isEqualTo(DEFAULT_TIPO_FACTURA);
        assertThat(testBoleta.getDiametro()).isEqualTo(DEFAULT_DIAMETRO);
        assertThat(testBoleta.getFechaVencimiento()).isEqualTo(DEFAULT_FECHA_VENCIMIENTO);
        assertThat(testBoleta.getFechaEmision()).isEqualTo(DEFAULT_FECHA_EMISION);
        assertThat(testBoleta.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testBoleta.getLastUpdDate()).isEqualTo(DEFAULT_LAST_UPD_DATE);
    }

    @Test
    @Transactional
    public void createBoletaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = boletaRepository.findAll().size();

        // Create the Boleta with an existing ID
        boleta.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        // Validate the Boleta in the database
        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNumeroBoletaIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setNumeroBoleta(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkConsumoCalculadoIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setConsumoCalculado(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkConsumoFacturadoIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setConsumoFacturado(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCargoFijoIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setCargoFijo(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkConsumoAguaPotableIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setConsumoAguaPotable(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkServAlcantarilladoIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setServAlcantarillado(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTratAguasServidasIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setTratAguasServidas(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSubConsumoMesIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setSubConsumoMes(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDespachoPostalCerIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setDespachoPostalCer(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkInteresesIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setIntereses(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAjusteSencilloAnteIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setAjusteSencilloAnte(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAjusteSencilloIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setAjusteSencillo(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMontoTotalIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setMontoTotal(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSaldoAnteriorIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setSaldoAnterior(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTotalPagarIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setTotalPagar(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFactorCobroIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setFactorCobro(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTipoFacturaIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setTipoFactura(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFechaVencimientoIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setFechaVencimiento(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFechaEmisionIsRequired() throws Exception {
        int databaseSizeBeforeTest = boletaRepository.findAll().size();
        // set the field null
        boleta.setFechaEmision(null);

        // Create the Boleta, which fails.

        restBoletaMockMvc.perform(post("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBoletas() throws Exception {
        // Initialize the database
        boletaRepository.saveAndFlush(boleta);

        // Get all the boletaList
        restBoletaMockMvc.perform(get("/api/boletas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(boleta.getId().intValue())))
            .andExpect(jsonPath("$.[*].numeroBoleta").value(hasItem(DEFAULT_NUMERO_BOLETA.intValue())))
            .andExpect(jsonPath("$.[*].consumoCalculado").value(hasItem(DEFAULT_CONSUMO_CALCULADO)))
            .andExpect(jsonPath("$.[*].consumoFacturado").value(hasItem(DEFAULT_CONSUMO_FACTURADO)))
            .andExpect(jsonPath("$.[*].cargoFijo").value(hasItem(DEFAULT_CARGO_FIJO)))
            .andExpect(jsonPath("$.[*].consumoAguaPotable").value(hasItem(DEFAULT_CONSUMO_AGUA_POTABLE.intValue())))
            .andExpect(jsonPath("$.[*].servAlcantarillado").value(hasItem(DEFAULT_SERV_ALCANTARILLADO.intValue())))
            .andExpect(jsonPath("$.[*].tratAguasServidas").value(hasItem(DEFAULT_TRAT_AGUAS_SERVIDAS.intValue())))
            .andExpect(jsonPath("$.[*].subConsumoMes").value(hasItem(DEFAULT_SUB_CONSUMO_MES.intValue())))
            .andExpect(jsonPath("$.[*].despachoPostalCer").value(hasItem(DEFAULT_DESPACHO_POSTAL_CER.intValue())))
            .andExpect(jsonPath("$.[*].intereses").value(hasItem(DEFAULT_INTERESES.intValue())))
            .andExpect(jsonPath("$.[*].ajusteSencilloAnte").value(hasItem(DEFAULT_AJUSTE_SENCILLO_ANTE.intValue())))
            .andExpect(jsonPath("$.[*].ajusteSencillo").value(hasItem(DEFAULT_AJUSTE_SENCILLO.intValue())))
            .andExpect(jsonPath("$.[*].montoTotal").value(hasItem(DEFAULT_MONTO_TOTAL.intValue())))
            .andExpect(jsonPath("$.[*].saldoAnterior").value(hasItem(DEFAULT_SALDO_ANTERIOR.intValue())))
            .andExpect(jsonPath("$.[*].totalPagar").value(hasItem(DEFAULT_TOTAL_PAGAR.intValue())))
            .andExpect(jsonPath("$.[*].tipoDespacho").value(hasItem(DEFAULT_TIPO_DESPACHO.toString())))
            .andExpect(jsonPath("$.[*].grupoTarifario").value(hasItem(DEFAULT_GRUPO_TARIFARIO.toString())))
            .andExpect(jsonPath("$.[*].factorCobro").value(hasItem(DEFAULT_FACTOR_COBRO.doubleValue())))
            .andExpect(jsonPath("$.[*].tipoFactura").value(hasItem(DEFAULT_TIPO_FACTURA.toString())))
            .andExpect(jsonPath("$.[*].diametro").value(hasItem(DEFAULT_DIAMETRO)))
            .andExpect(jsonPath("$.[*].fechaVencimiento").value(hasItem(DEFAULT_FECHA_VENCIMIENTO.toString())))
            .andExpect(jsonPath("$.[*].fechaEmision").value(hasItem(DEFAULT_FECHA_EMISION.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastUpdDate").value(hasItem(DEFAULT_LAST_UPD_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getBoleta() throws Exception {
        // Initialize the database
        boletaRepository.saveAndFlush(boleta);

        // Get the boleta
        restBoletaMockMvc.perform(get("/api/boletas/{id}", boleta.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(boleta.getId().intValue()))
            .andExpect(jsonPath("$.numeroBoleta").value(DEFAULT_NUMERO_BOLETA.intValue()))
            .andExpect(jsonPath("$.consumoCalculado").value(DEFAULT_CONSUMO_CALCULADO))
            .andExpect(jsonPath("$.consumoFacturado").value(DEFAULT_CONSUMO_FACTURADO))
            .andExpect(jsonPath("$.cargoFijo").value(DEFAULT_CARGO_FIJO))
            .andExpect(jsonPath("$.consumoAguaPotable").value(DEFAULT_CONSUMO_AGUA_POTABLE.intValue()))
            .andExpect(jsonPath("$.servAlcantarillado").value(DEFAULT_SERV_ALCANTARILLADO.intValue()))
            .andExpect(jsonPath("$.tratAguasServidas").value(DEFAULT_TRAT_AGUAS_SERVIDAS.intValue()))
            .andExpect(jsonPath("$.subConsumoMes").value(DEFAULT_SUB_CONSUMO_MES.intValue()))
            .andExpect(jsonPath("$.despachoPostalCer").value(DEFAULT_DESPACHO_POSTAL_CER.intValue()))
            .andExpect(jsonPath("$.intereses").value(DEFAULT_INTERESES.intValue()))
            .andExpect(jsonPath("$.ajusteSencilloAnte").value(DEFAULT_AJUSTE_SENCILLO_ANTE.intValue()))
            .andExpect(jsonPath("$.ajusteSencillo").value(DEFAULT_AJUSTE_SENCILLO.intValue()))
            .andExpect(jsonPath("$.montoTotal").value(DEFAULT_MONTO_TOTAL.intValue()))
            .andExpect(jsonPath("$.saldoAnterior").value(DEFAULT_SALDO_ANTERIOR.intValue()))
            .andExpect(jsonPath("$.totalPagar").value(DEFAULT_TOTAL_PAGAR.intValue()))
            .andExpect(jsonPath("$.tipoDespacho").value(DEFAULT_TIPO_DESPACHO.toString()))
            .andExpect(jsonPath("$.grupoTarifario").value(DEFAULT_GRUPO_TARIFARIO.toString()))
            .andExpect(jsonPath("$.factorCobro").value(DEFAULT_FACTOR_COBRO.doubleValue()))
            .andExpect(jsonPath("$.tipoFactura").value(DEFAULT_TIPO_FACTURA.toString()))
            .andExpect(jsonPath("$.diametro").value(DEFAULT_DIAMETRO))
            .andExpect(jsonPath("$.fechaVencimiento").value(DEFAULT_FECHA_VENCIMIENTO.toString()))
            .andExpect(jsonPath("$.fechaEmision").value(DEFAULT_FECHA_EMISION.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastUpdDate").value(DEFAULT_LAST_UPD_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingBoleta() throws Exception {
        // Get the boleta
        restBoletaMockMvc.perform(get("/api/boletas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBoleta() throws Exception {
        // Initialize the database
        boletaRepository.saveAndFlush(boleta);

        int databaseSizeBeforeUpdate = boletaRepository.findAll().size();

        // Update the boleta
        Boleta updatedBoleta = boletaRepository.findById(boleta.getId()).get();
        // Disconnect from session so that the updates on updatedBoleta are not directly saved in db
        em.detach(updatedBoleta);
        updatedBoleta
            .numeroBoleta(UPDATED_NUMERO_BOLETA)
            .consumoCalculado(UPDATED_CONSUMO_CALCULADO)
            .consumoFacturado(UPDATED_CONSUMO_FACTURADO)
            .cargoFijo(UPDATED_CARGO_FIJO)
            .consumoAguaPotable(UPDATED_CONSUMO_AGUA_POTABLE)
            .servAlcantarillado(UPDATED_SERV_ALCANTARILLADO)
            .tratAguasServidas(UPDATED_TRAT_AGUAS_SERVIDAS)
            .subConsumoMes(UPDATED_SUB_CONSUMO_MES)
            .despachoPostalCer(UPDATED_DESPACHO_POSTAL_CER)
            .intereses(UPDATED_INTERESES)
            .ajusteSencilloAnte(UPDATED_AJUSTE_SENCILLO_ANTE)
            .ajusteSencillo(UPDATED_AJUSTE_SENCILLO)
            .montoTotal(UPDATED_MONTO_TOTAL)
            .saldoAnterior(UPDATED_SALDO_ANTERIOR)
            .totalPagar(UPDATED_TOTAL_PAGAR)
            .tipoDespacho(UPDATED_TIPO_DESPACHO)
            .grupoTarifario(UPDATED_GRUPO_TARIFARIO)
            .factorCobro(UPDATED_FACTOR_COBRO)
            .tipoFactura(UPDATED_TIPO_FACTURA)
            .diametro(UPDATED_DIAMETRO)
            .fechaVencimiento(UPDATED_FECHA_VENCIMIENTO)
            .fechaEmision(UPDATED_FECHA_EMISION)
            .createdDate(UPDATED_CREATED_DATE)
            .lastUpdDate(UPDATED_LAST_UPD_DATE);

        restBoletaMockMvc.perform(put("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBoleta)))
            .andExpect(status().isOk());

        // Validate the Boleta in the database
        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeUpdate);
        Boleta testBoleta = boletaList.get(boletaList.size() - 1);
        assertThat(testBoleta.getNumeroBoleta()).isEqualTo(UPDATED_NUMERO_BOLETA);
        assertThat(testBoleta.getConsumoCalculado()).isEqualTo(UPDATED_CONSUMO_CALCULADO);
        assertThat(testBoleta.getConsumoFacturado()).isEqualTo(UPDATED_CONSUMO_FACTURADO);
        assertThat(testBoleta.getCargoFijo()).isEqualTo(UPDATED_CARGO_FIJO);
        assertThat(testBoleta.getConsumoAguaPotable()).isEqualTo(UPDATED_CONSUMO_AGUA_POTABLE);
        assertThat(testBoleta.getServAlcantarillado()).isEqualTo(UPDATED_SERV_ALCANTARILLADO);
        assertThat(testBoleta.getTratAguasServidas()).isEqualTo(UPDATED_TRAT_AGUAS_SERVIDAS);
        assertThat(testBoleta.getSubConsumoMes()).isEqualTo(UPDATED_SUB_CONSUMO_MES);
        assertThat(testBoleta.getDespachoPostalCer()).isEqualTo(UPDATED_DESPACHO_POSTAL_CER);
        assertThat(testBoleta.getIntereses()).isEqualTo(UPDATED_INTERESES);
        assertThat(testBoleta.getAjusteSencilloAnte()).isEqualTo(UPDATED_AJUSTE_SENCILLO_ANTE);
        assertThat(testBoleta.getAjusteSencillo()).isEqualTo(UPDATED_AJUSTE_SENCILLO);
        assertThat(testBoleta.getMontoTotal()).isEqualTo(UPDATED_MONTO_TOTAL);
        assertThat(testBoleta.getSaldoAnterior()).isEqualTo(UPDATED_SALDO_ANTERIOR);
        assertThat(testBoleta.getTotalPagar()).isEqualTo(UPDATED_TOTAL_PAGAR);
        assertThat(testBoleta.getTipoDespacho()).isEqualTo(UPDATED_TIPO_DESPACHO);
        assertThat(testBoleta.getGrupoTarifario()).isEqualTo(UPDATED_GRUPO_TARIFARIO);
        assertThat(testBoleta.getFactorCobro()).isEqualTo(UPDATED_FACTOR_COBRO);
        assertThat(testBoleta.getTipoFactura()).isEqualTo(UPDATED_TIPO_FACTURA);
        assertThat(testBoleta.getDiametro()).isEqualTo(UPDATED_DIAMETRO);
        assertThat(testBoleta.getFechaVencimiento()).isEqualTo(UPDATED_FECHA_VENCIMIENTO);
        assertThat(testBoleta.getFechaEmision()).isEqualTo(UPDATED_FECHA_EMISION);
        assertThat(testBoleta.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testBoleta.getLastUpdDate()).isEqualTo(UPDATED_LAST_UPD_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingBoleta() throws Exception {
        int databaseSizeBeforeUpdate = boletaRepository.findAll().size();

        // Create the Boleta

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBoletaMockMvc.perform(put("/api/boletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boleta)))
            .andExpect(status().isBadRequest());

        // Validate the Boleta in the database
        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBoleta() throws Exception {
        // Initialize the database
        boletaRepository.saveAndFlush(boleta);

        int databaseSizeBeforeDelete = boletaRepository.findAll().size();

        // Delete the boleta
        restBoletaMockMvc.perform(delete("/api/boletas/{id}", boleta.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Boleta> boletaList = boletaRepository.findAll();
        assertThat(boletaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Boleta.class);
        Boleta boleta1 = new Boleta();
        boleta1.setId(1L);
        Boleta boleta2 = new Boleta();
        boleta2.setId(boleta1.getId());
        assertThat(boleta1).isEqualTo(boleta2);
        boleta2.setId(2L);
        assertThat(boleta1).isNotEqualTo(boleta2);
        boleta1.setId(null);
        assertThat(boleta1).isNotEqualTo(boleta2);
    }
}
