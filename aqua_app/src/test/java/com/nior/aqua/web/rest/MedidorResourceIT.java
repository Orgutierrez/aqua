package com.nior.aqua.web.rest;

import com.nior.aqua.AquaAppApp;
import com.nior.aqua.domain.Medidor;
import com.nior.aqua.domain.Direccion;
import com.nior.aqua.repository.MedidorRepository;
import com.nior.aqua.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.nior.aqua.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MedidorResource} REST controller.
 */
@SpringBootTest(classes = AquaAppApp.class)
public class MedidorResourceIT {

    private static final String DEFAULT_IDENTIFICADOR = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIFICADOR = "BBBBBBBBBB";

    private static final String DEFAULT_MARCA = "AAAAAAAAAA";
    private static final String UPDATED_MARCA = "BBBBBBBBBB";

    private static final String DEFAULT_MODELO = "AAAAAAAAAA";
    private static final String UPDATED_MODELO = "BBBBBBBBBB";

    private static final Long DEFAULT_INICIO_CONTADOR = 1L;
    private static final Long UPDATED_INICIO_CONTADOR = 2L;
    private static final Long SMALLER_INICIO_CONTADOR = 1L - 1L;

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_CREATED_DATE = Instant.ofEpochMilli(-1L);

    private static final Instant DEFAULT_LAST_UPD_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_UPD_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_LAST_UPD_DATE = Instant.ofEpochMilli(-1L);

    @Autowired
    private MedidorRepository medidorRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMedidorMockMvc;

    private Medidor medidor;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MedidorResource medidorResource = new MedidorResource(medidorRepository);
        this.restMedidorMockMvc = MockMvcBuilders.standaloneSetup(medidorResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Medidor createEntity(EntityManager em) {
        Medidor medidor = new Medidor()
            .identificador(DEFAULT_IDENTIFICADOR)
            .marca(DEFAULT_MARCA)
            .modelo(DEFAULT_MODELO)
            .inicioContador(DEFAULT_INICIO_CONTADOR)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastUpdDate(DEFAULT_LAST_UPD_DATE);
        // Add required entity
        Direccion direccion;
        if (TestUtil.findAll(em, Direccion.class).isEmpty()) {
            direccion = DireccionResourceIT.createEntity(em);
            em.persist(direccion);
            em.flush();
        } else {
            direccion = TestUtil.findAll(em, Direccion.class).get(0);
        }
        medidor.setDireccion(direccion);
        return medidor;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Medidor createUpdatedEntity(EntityManager em) {
        Medidor medidor = new Medidor()
            .identificador(UPDATED_IDENTIFICADOR)
            .marca(UPDATED_MARCA)
            .modelo(UPDATED_MODELO)
            .inicioContador(UPDATED_INICIO_CONTADOR)
            .createdDate(UPDATED_CREATED_DATE)
            .lastUpdDate(UPDATED_LAST_UPD_DATE);
        // Add required entity
        Direccion direccion;
        if (TestUtil.findAll(em, Direccion.class).isEmpty()) {
            direccion = DireccionResourceIT.createUpdatedEntity(em);
            em.persist(direccion);
            em.flush();
        } else {
            direccion = TestUtil.findAll(em, Direccion.class).get(0);
        }
        medidor.setDireccion(direccion);
        return medidor;
    }

    @BeforeEach
    public void initTest() {
        medidor = createEntity(em);
    }

    @Test
    @Transactional
    public void createMedidor() throws Exception {
        int databaseSizeBeforeCreate = medidorRepository.findAll().size();

        // Create the Medidor
        restMedidorMockMvc.perform(post("/api/medidors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(medidor)))
            .andExpect(status().isCreated());

        // Validate the Medidor in the database
        List<Medidor> medidorList = medidorRepository.findAll();
        assertThat(medidorList).hasSize(databaseSizeBeforeCreate + 1);
        Medidor testMedidor = medidorList.get(medidorList.size() - 1);
        assertThat(testMedidor.getIdentificador()).isEqualTo(DEFAULT_IDENTIFICADOR);
        assertThat(testMedidor.getMarca()).isEqualTo(DEFAULT_MARCA);
        assertThat(testMedidor.getModelo()).isEqualTo(DEFAULT_MODELO);
        assertThat(testMedidor.getInicioContador()).isEqualTo(DEFAULT_INICIO_CONTADOR);
        assertThat(testMedidor.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testMedidor.getLastUpdDate()).isEqualTo(DEFAULT_LAST_UPD_DATE);
    }

    @Test
    @Transactional
    public void createMedidorWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = medidorRepository.findAll().size();

        // Create the Medidor with an existing ID
        medidor.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMedidorMockMvc.perform(post("/api/medidors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(medidor)))
            .andExpect(status().isBadRequest());

        // Validate the Medidor in the database
        List<Medidor> medidorList = medidorRepository.findAll();
        assertThat(medidorList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIdentificadorIsRequired() throws Exception {
        int databaseSizeBeforeTest = medidorRepository.findAll().size();
        // set the field null
        medidor.setIdentificador(null);

        // Create the Medidor, which fails.

        restMedidorMockMvc.perform(post("/api/medidors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(medidor)))
            .andExpect(status().isBadRequest());

        List<Medidor> medidorList = medidorRepository.findAll();
        assertThat(medidorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMarcaIsRequired() throws Exception {
        int databaseSizeBeforeTest = medidorRepository.findAll().size();
        // set the field null
        medidor.setMarca(null);

        // Create the Medidor, which fails.

        restMedidorMockMvc.perform(post("/api/medidors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(medidor)))
            .andExpect(status().isBadRequest());

        List<Medidor> medidorList = medidorRepository.findAll();
        assertThat(medidorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMedidors() throws Exception {
        // Initialize the database
        medidorRepository.saveAndFlush(medidor);

        // Get all the medidorList
        restMedidorMockMvc.perform(get("/api/medidors?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(medidor.getId().intValue())))
            .andExpect(jsonPath("$.[*].identificador").value(hasItem(DEFAULT_IDENTIFICADOR.toString())))
            .andExpect(jsonPath("$.[*].marca").value(hasItem(DEFAULT_MARCA.toString())))
            .andExpect(jsonPath("$.[*].modelo").value(hasItem(DEFAULT_MODELO.toString())))
            .andExpect(jsonPath("$.[*].inicioContador").value(hasItem(DEFAULT_INICIO_CONTADOR.intValue())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastUpdDate").value(hasItem(DEFAULT_LAST_UPD_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getMedidor() throws Exception {
        // Initialize the database
        medidorRepository.saveAndFlush(medidor);

        // Get the medidor
        restMedidorMockMvc.perform(get("/api/medidors/{id}", medidor.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(medidor.getId().intValue()))
            .andExpect(jsonPath("$.identificador").value(DEFAULT_IDENTIFICADOR.toString()))
            .andExpect(jsonPath("$.marca").value(DEFAULT_MARCA.toString()))
            .andExpect(jsonPath("$.modelo").value(DEFAULT_MODELO.toString()))
            .andExpect(jsonPath("$.inicioContador").value(DEFAULT_INICIO_CONTADOR.intValue()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastUpdDate").value(DEFAULT_LAST_UPD_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMedidor() throws Exception {
        // Get the medidor
        restMedidorMockMvc.perform(get("/api/medidors/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMedidor() throws Exception {
        // Initialize the database
        medidorRepository.saveAndFlush(medidor);

        int databaseSizeBeforeUpdate = medidorRepository.findAll().size();

        // Update the medidor
        Medidor updatedMedidor = medidorRepository.findById(medidor.getId()).get();
        // Disconnect from session so that the updates on updatedMedidor are not directly saved in db
        em.detach(updatedMedidor);
        updatedMedidor
            .identificador(UPDATED_IDENTIFICADOR)
            .marca(UPDATED_MARCA)
            .modelo(UPDATED_MODELO)
            .inicioContador(UPDATED_INICIO_CONTADOR)
            .createdDate(UPDATED_CREATED_DATE)
            .lastUpdDate(UPDATED_LAST_UPD_DATE);

        restMedidorMockMvc.perform(put("/api/medidors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMedidor)))
            .andExpect(status().isOk());

        // Validate the Medidor in the database
        List<Medidor> medidorList = medidorRepository.findAll();
        assertThat(medidorList).hasSize(databaseSizeBeforeUpdate);
        Medidor testMedidor = medidorList.get(medidorList.size() - 1);
        assertThat(testMedidor.getIdentificador()).isEqualTo(UPDATED_IDENTIFICADOR);
        assertThat(testMedidor.getMarca()).isEqualTo(UPDATED_MARCA);
        assertThat(testMedidor.getModelo()).isEqualTo(UPDATED_MODELO);
        assertThat(testMedidor.getInicioContador()).isEqualTo(UPDATED_INICIO_CONTADOR);
        assertThat(testMedidor.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testMedidor.getLastUpdDate()).isEqualTo(UPDATED_LAST_UPD_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingMedidor() throws Exception {
        int databaseSizeBeforeUpdate = medidorRepository.findAll().size();

        // Create the Medidor

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMedidorMockMvc.perform(put("/api/medidors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(medidor)))
            .andExpect(status().isBadRequest());

        // Validate the Medidor in the database
        List<Medidor> medidorList = medidorRepository.findAll();
        assertThat(medidorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMedidor() throws Exception {
        // Initialize the database
        medidorRepository.saveAndFlush(medidor);

        int databaseSizeBeforeDelete = medidorRepository.findAll().size();

        // Delete the medidor
        restMedidorMockMvc.perform(delete("/api/medidors/{id}", medidor.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Medidor> medidorList = medidorRepository.findAll();
        assertThat(medidorList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Medidor.class);
        Medidor medidor1 = new Medidor();
        medidor1.setId(1L);
        Medidor medidor2 = new Medidor();
        medidor2.setId(medidor1.getId());
        assertThat(medidor1).isEqualTo(medidor2);
        medidor2.setId(2L);
        assertThat(medidor1).isNotEqualTo(medidor2);
        medidor1.setId(null);
        assertThat(medidor1).isNotEqualTo(medidor2);
    }
}
