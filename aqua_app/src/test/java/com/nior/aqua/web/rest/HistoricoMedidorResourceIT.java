package com.nior.aqua.web.rest;

import com.nior.aqua.AquaAppApp;
import com.nior.aqua.domain.HistoricoMedidor;
import com.nior.aqua.domain.Medidor;
import com.nior.aqua.repository.HistoricoMedidorRepository;
import com.nior.aqua.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.nior.aqua.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link HistoricoMedidorResource} REST controller.
 */
@SpringBootTest(classes = AquaAppApp.class)
public class HistoricoMedidorResourceIT {

    private static final Long DEFAULT_INICIO_CONTADOR = 1L;
    private static final Long UPDATED_INICIO_CONTADOR = 2L;
    private static final Long SMALLER_INICIO_CONTADOR = 1L - 1L;

    private static final Long DEFAULT_FIN_CONTADOR = 1L;
    private static final Long UPDATED_FIN_CONTADOR = 2L;
    private static final Long SMALLER_FIN_CONTADOR = 1L - 1L;

    private static final Instant DEFAULT_INICIO_PERIODO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_INICIO_PERIODO = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_INICIO_PERIODO = Instant.ofEpochMilli(-1L);

    private static final Instant DEFAULT_FIN_PERIODO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FIN_PERIODO = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_FIN_PERIODO = Instant.ofEpochMilli(-1L);

    @Autowired
    private HistoricoMedidorRepository historicoMedidorRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restHistoricoMedidorMockMvc;

    private HistoricoMedidor historicoMedidor;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final HistoricoMedidorResource historicoMedidorResource = new HistoricoMedidorResource(historicoMedidorRepository);
        this.restHistoricoMedidorMockMvc = MockMvcBuilders.standaloneSetup(historicoMedidorResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HistoricoMedidor createEntity(EntityManager em) {
        HistoricoMedidor historicoMedidor = new HistoricoMedidor()
            .inicioContador(DEFAULT_INICIO_CONTADOR)
            .finContador(DEFAULT_FIN_CONTADOR)
            .inicioPeriodo(DEFAULT_INICIO_PERIODO)
            .finPeriodo(DEFAULT_FIN_PERIODO);
        // Add required entity
        Medidor medidor;
        if (TestUtil.findAll(em, Medidor.class).isEmpty()) {
            medidor = MedidorResourceIT.createEntity(em);
            em.persist(medidor);
            em.flush();
        } else {
            medidor = TestUtil.findAll(em, Medidor.class).get(0);
        }
        historicoMedidor.setMedidor(medidor);
        return historicoMedidor;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HistoricoMedidor createUpdatedEntity(EntityManager em) {
        HistoricoMedidor historicoMedidor = new HistoricoMedidor()
            .inicioContador(UPDATED_INICIO_CONTADOR)
            .finContador(UPDATED_FIN_CONTADOR)
            .inicioPeriodo(UPDATED_INICIO_PERIODO)
            .finPeriodo(UPDATED_FIN_PERIODO);
        // Add required entity
        Medidor medidor;
        if (TestUtil.findAll(em, Medidor.class).isEmpty()) {
            medidor = MedidorResourceIT.createUpdatedEntity(em);
            em.persist(medidor);
            em.flush();
        } else {
            medidor = TestUtil.findAll(em, Medidor.class).get(0);
        }
        historicoMedidor.setMedidor(medidor);
        return historicoMedidor;
    }

    @BeforeEach
    public void initTest() {
        historicoMedidor = createEntity(em);
    }

    @Test
    @Transactional
    public void createHistoricoMedidor() throws Exception {
        int databaseSizeBeforeCreate = historicoMedidorRepository.findAll().size();

        // Create the HistoricoMedidor
        restHistoricoMedidorMockMvc.perform(post("/api/historico-medidors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(historicoMedidor)))
            .andExpect(status().isCreated());

        // Validate the HistoricoMedidor in the database
        List<HistoricoMedidor> historicoMedidorList = historicoMedidorRepository.findAll();
        assertThat(historicoMedidorList).hasSize(databaseSizeBeforeCreate + 1);
        HistoricoMedidor testHistoricoMedidor = historicoMedidorList.get(historicoMedidorList.size() - 1);
        assertThat(testHistoricoMedidor.getInicioContador()).isEqualTo(DEFAULT_INICIO_CONTADOR);
        assertThat(testHistoricoMedidor.getFinContador()).isEqualTo(DEFAULT_FIN_CONTADOR);
        assertThat(testHistoricoMedidor.getInicioPeriodo()).isEqualTo(DEFAULT_INICIO_PERIODO);
        assertThat(testHistoricoMedidor.getFinPeriodo()).isEqualTo(DEFAULT_FIN_PERIODO);
    }

    @Test
    @Transactional
    public void createHistoricoMedidorWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = historicoMedidorRepository.findAll().size();

        // Create the HistoricoMedidor with an existing ID
        historicoMedidor.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHistoricoMedidorMockMvc.perform(post("/api/historico-medidors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(historicoMedidor)))
            .andExpect(status().isBadRequest());

        // Validate the HistoricoMedidor in the database
        List<HistoricoMedidor> historicoMedidorList = historicoMedidorRepository.findAll();
        assertThat(historicoMedidorList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkInicioContadorIsRequired() throws Exception {
        int databaseSizeBeforeTest = historicoMedidorRepository.findAll().size();
        // set the field null
        historicoMedidor.setInicioContador(null);

        // Create the HistoricoMedidor, which fails.

        restHistoricoMedidorMockMvc.perform(post("/api/historico-medidors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(historicoMedidor)))
            .andExpect(status().isBadRequest());

        List<HistoricoMedidor> historicoMedidorList = historicoMedidorRepository.findAll();
        assertThat(historicoMedidorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFinContadorIsRequired() throws Exception {
        int databaseSizeBeforeTest = historicoMedidorRepository.findAll().size();
        // set the field null
        historicoMedidor.setFinContador(null);

        // Create the HistoricoMedidor, which fails.

        restHistoricoMedidorMockMvc.perform(post("/api/historico-medidors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(historicoMedidor)))
            .andExpect(status().isBadRequest());

        List<HistoricoMedidor> historicoMedidorList = historicoMedidorRepository.findAll();
        assertThat(historicoMedidorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkInicioPeriodoIsRequired() throws Exception {
        int databaseSizeBeforeTest = historicoMedidorRepository.findAll().size();
        // set the field null
        historicoMedidor.setInicioPeriodo(null);

        // Create the HistoricoMedidor, which fails.

        restHistoricoMedidorMockMvc.perform(post("/api/historico-medidors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(historicoMedidor)))
            .andExpect(status().isBadRequest());

        List<HistoricoMedidor> historicoMedidorList = historicoMedidorRepository.findAll();
        assertThat(historicoMedidorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFinPeriodoIsRequired() throws Exception {
        int databaseSizeBeforeTest = historicoMedidorRepository.findAll().size();
        // set the field null
        historicoMedidor.setFinPeriodo(null);

        // Create the HistoricoMedidor, which fails.

        restHistoricoMedidorMockMvc.perform(post("/api/historico-medidors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(historicoMedidor)))
            .andExpect(status().isBadRequest());

        List<HistoricoMedidor> historicoMedidorList = historicoMedidorRepository.findAll();
        assertThat(historicoMedidorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllHistoricoMedidors() throws Exception {
        // Initialize the database
        historicoMedidorRepository.saveAndFlush(historicoMedidor);

        // Get all the historicoMedidorList
        restHistoricoMedidorMockMvc.perform(get("/api/historico-medidors?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(historicoMedidor.getId().intValue())))
            .andExpect(jsonPath("$.[*].inicioContador").value(hasItem(DEFAULT_INICIO_CONTADOR.intValue())))
            .andExpect(jsonPath("$.[*].finContador").value(hasItem(DEFAULT_FIN_CONTADOR.intValue())))
            .andExpect(jsonPath("$.[*].inicioPeriodo").value(hasItem(DEFAULT_INICIO_PERIODO.toString())))
            .andExpect(jsonPath("$.[*].finPeriodo").value(hasItem(DEFAULT_FIN_PERIODO.toString())));
    }
    
    @Test
    @Transactional
    public void getHistoricoMedidor() throws Exception {
        // Initialize the database
        historicoMedidorRepository.saveAndFlush(historicoMedidor);

        // Get the historicoMedidor
        restHistoricoMedidorMockMvc.perform(get("/api/historico-medidors/{id}", historicoMedidor.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(historicoMedidor.getId().intValue()))
            .andExpect(jsonPath("$.inicioContador").value(DEFAULT_INICIO_CONTADOR.intValue()))
            .andExpect(jsonPath("$.finContador").value(DEFAULT_FIN_CONTADOR.intValue()))
            .andExpect(jsonPath("$.inicioPeriodo").value(DEFAULT_INICIO_PERIODO.toString()))
            .andExpect(jsonPath("$.finPeriodo").value(DEFAULT_FIN_PERIODO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingHistoricoMedidor() throws Exception {
        // Get the historicoMedidor
        restHistoricoMedidorMockMvc.perform(get("/api/historico-medidors/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHistoricoMedidor() throws Exception {
        // Initialize the database
        historicoMedidorRepository.saveAndFlush(historicoMedidor);

        int databaseSizeBeforeUpdate = historicoMedidorRepository.findAll().size();

        // Update the historicoMedidor
        HistoricoMedidor updatedHistoricoMedidor = historicoMedidorRepository.findById(historicoMedidor.getId()).get();
        // Disconnect from session so that the updates on updatedHistoricoMedidor are not directly saved in db
        em.detach(updatedHistoricoMedidor);
        updatedHistoricoMedidor
            .inicioContador(UPDATED_INICIO_CONTADOR)
            .finContador(UPDATED_FIN_CONTADOR)
            .inicioPeriodo(UPDATED_INICIO_PERIODO)
            .finPeriodo(UPDATED_FIN_PERIODO);

        restHistoricoMedidorMockMvc.perform(put("/api/historico-medidors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedHistoricoMedidor)))
            .andExpect(status().isOk());

        // Validate the HistoricoMedidor in the database
        List<HistoricoMedidor> historicoMedidorList = historicoMedidorRepository.findAll();
        assertThat(historicoMedidorList).hasSize(databaseSizeBeforeUpdate);
        HistoricoMedidor testHistoricoMedidor = historicoMedidorList.get(historicoMedidorList.size() - 1);
        assertThat(testHistoricoMedidor.getInicioContador()).isEqualTo(UPDATED_INICIO_CONTADOR);
        assertThat(testHistoricoMedidor.getFinContador()).isEqualTo(UPDATED_FIN_CONTADOR);
        assertThat(testHistoricoMedidor.getInicioPeriodo()).isEqualTo(UPDATED_INICIO_PERIODO);
        assertThat(testHistoricoMedidor.getFinPeriodo()).isEqualTo(UPDATED_FIN_PERIODO);
    }

    @Test
    @Transactional
    public void updateNonExistingHistoricoMedidor() throws Exception {
        int databaseSizeBeforeUpdate = historicoMedidorRepository.findAll().size();

        // Create the HistoricoMedidor

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHistoricoMedidorMockMvc.perform(put("/api/historico-medidors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(historicoMedidor)))
            .andExpect(status().isBadRequest());

        // Validate the HistoricoMedidor in the database
        List<HistoricoMedidor> historicoMedidorList = historicoMedidorRepository.findAll();
        assertThat(historicoMedidorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteHistoricoMedidor() throws Exception {
        // Initialize the database
        historicoMedidorRepository.saveAndFlush(historicoMedidor);

        int databaseSizeBeforeDelete = historicoMedidorRepository.findAll().size();

        // Delete the historicoMedidor
        restHistoricoMedidorMockMvc.perform(delete("/api/historico-medidors/{id}", historicoMedidor.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<HistoricoMedidor> historicoMedidorList = historicoMedidorRepository.findAll();
        assertThat(historicoMedidorList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HistoricoMedidor.class);
        HistoricoMedidor historicoMedidor1 = new HistoricoMedidor();
        historicoMedidor1.setId(1L);
        HistoricoMedidor historicoMedidor2 = new HistoricoMedidor();
        historicoMedidor2.setId(historicoMedidor1.getId());
        assertThat(historicoMedidor1).isEqualTo(historicoMedidor2);
        historicoMedidor2.setId(2L);
        assertThat(historicoMedidor1).isNotEqualTo(historicoMedidor2);
        historicoMedidor1.setId(null);
        assertThat(historicoMedidor1).isNotEqualTo(historicoMedidor2);
    }
}
