package com.nior.aqua.repository;

import com.nior.aqua.domain.HistoricoMedidor;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the HistoricoMedidor entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HistoricoMedidorRepository extends JpaRepository<HistoricoMedidor, Long> {

}
