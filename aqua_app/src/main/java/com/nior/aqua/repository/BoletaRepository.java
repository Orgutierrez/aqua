package com.nior.aqua.repository;

import com.nior.aqua.domain.Boleta;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Boleta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BoletaRepository extends JpaRepository<Boleta, Long> {

}
