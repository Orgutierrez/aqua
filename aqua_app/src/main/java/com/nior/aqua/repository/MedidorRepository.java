package com.nior.aqua.repository;

import com.nior.aqua.domain.Medidor;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Medidor entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MedidorRepository extends JpaRepository<Medidor, Long> {

}
