package com.nior.aqua.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A Boleta.
 */
@Entity
@Table(name = "boleta")
public class Boleta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "numero_boleta", nullable = false, unique = true)
    private Long numeroBoleta;

    @NotNull
    @Column(name = "consumo_calculado", nullable = false)
    private Integer consumoCalculado;

    @NotNull
    @Column(name = "consumo_facturado", nullable = false)
    private Integer consumoFacturado;

    @NotNull
    @Column(name = "cargo_fijo", nullable = false)
    private Integer cargoFijo;

    @NotNull
    @Column(name = "consumo_agua_potable", nullable = false)
    private Long consumoAguaPotable;

    @NotNull
    @Column(name = "serv_alcantarillado", nullable = false)
    private Long servAlcantarillado;

    @NotNull
    @Column(name = "trat_aguas_servidas", nullable = false)
    private Long tratAguasServidas;

    @NotNull
    @Column(name = "sub_consumo_mes", nullable = false)
    private Long subConsumoMes;

    @NotNull
    @Column(name = "despacho_postal_cer", nullable = false)
    private Long despachoPostalCer;

    @NotNull
    @Column(name = "intereses", nullable = false)
    private Long intereses;

    @NotNull
    @Column(name = "ajuste_sencillo_ante", nullable = false)
    private Long ajusteSencilloAnte;

    @NotNull
    @Column(name = "ajuste_sencillo", nullable = false)
    private Long ajusteSencillo;

    @NotNull
    @Column(name = "monto_total", nullable = false)
    private Long montoTotal;

    @NotNull
    @Column(name = "saldo_anterior", nullable = false)
    private Long saldoAnterior;

    @NotNull
    @Column(name = "total_pagar", nullable = false)
    private Long totalPagar;

    @Column(name = "tipo_despacho")
    private String tipoDespacho;

    @Column(name = "grupo_tarifario")
    private String grupoTarifario;

    @NotNull
    @Column(name = "factor_cobro", nullable = false)
    private Float factorCobro;

    @NotNull
    @Column(name = "tipo_factura", nullable = false)
    private String tipoFactura;

    @Column(name = "diametro")
    private Integer diametro;

    @NotNull
    @Column(name = "fecha_vencimiento", nullable = false)
    private Instant fechaVencimiento;

    @NotNull
    @Column(name = "fecha_emision", nullable = false)
    private Instant fechaEmision;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "last_upd_date")
    private Instant lastUpdDate;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("boletas")
    private Medidor medidor;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumeroBoleta() {
        return numeroBoleta;
    }

    public Boleta numeroBoleta(Long numeroBoleta) {
        this.numeroBoleta = numeroBoleta;
        return this;
    }

    public void setNumeroBoleta(Long numeroBoleta) {
        this.numeroBoleta = numeroBoleta;
    }

    public Integer getConsumoCalculado() {
        return consumoCalculado;
    }

    public Boleta consumoCalculado(Integer consumoCalculado) {
        this.consumoCalculado = consumoCalculado;
        return this;
    }

    public void setConsumoCalculado(Integer consumoCalculado) {
        this.consumoCalculado = consumoCalculado;
    }

    public Integer getConsumoFacturado() {
        return consumoFacturado;
    }

    public Boleta consumoFacturado(Integer consumoFacturado) {
        this.consumoFacturado = consumoFacturado;
        return this;
    }

    public void setConsumoFacturado(Integer consumoFacturado) {
        this.consumoFacturado = consumoFacturado;
    }

    public Integer getCargoFijo() {
        return cargoFijo;
    }

    public Boleta cargoFijo(Integer cargoFijo) {
        this.cargoFijo = cargoFijo;
        return this;
    }

    public void setCargoFijo(Integer cargoFijo) {
        this.cargoFijo = cargoFijo;
    }

    public Long getConsumoAguaPotable() {
        return consumoAguaPotable;
    }

    public Boleta consumoAguaPotable(Long consumoAguaPotable) {
        this.consumoAguaPotable = consumoAguaPotable;
        return this;
    }

    public void setConsumoAguaPotable(Long consumoAguaPotable) {
        this.consumoAguaPotable = consumoAguaPotable;
    }

    public Long getServAlcantarillado() {
        return servAlcantarillado;
    }

    public Boleta servAlcantarillado(Long servAlcantarillado) {
        this.servAlcantarillado = servAlcantarillado;
        return this;
    }

    public void setServAlcantarillado(Long servAlcantarillado) {
        this.servAlcantarillado = servAlcantarillado;
    }

    public Long getTratAguasServidas() {
        return tratAguasServidas;
    }

    public Boleta tratAguasServidas(Long tratAguasServidas) {
        this.tratAguasServidas = tratAguasServidas;
        return this;
    }

    public void setTratAguasServidas(Long tratAguasServidas) {
        this.tratAguasServidas = tratAguasServidas;
    }

    public Long getSubConsumoMes() {
        return subConsumoMes;
    }

    public Boleta subConsumoMes(Long subConsumoMes) {
        this.subConsumoMes = subConsumoMes;
        return this;
    }

    public void setSubConsumoMes(Long subConsumoMes) {
        this.subConsumoMes = subConsumoMes;
    }

    public Long getDespachoPostalCer() {
        return despachoPostalCer;
    }

    public Boleta despachoPostalCer(Long despachoPostalCer) {
        this.despachoPostalCer = despachoPostalCer;
        return this;
    }

    public void setDespachoPostalCer(Long despachoPostalCer) {
        this.despachoPostalCer = despachoPostalCer;
    }

    public Long getIntereses() {
        return intereses;
    }

    public Boleta intereses(Long intereses) {
        this.intereses = intereses;
        return this;
    }

    public void setIntereses(Long intereses) {
        this.intereses = intereses;
    }

    public Long getAjusteSencilloAnte() {
        return ajusteSencilloAnte;
    }

    public Boleta ajusteSencilloAnte(Long ajusteSencilloAnte) {
        this.ajusteSencilloAnte = ajusteSencilloAnte;
        return this;
    }

    public void setAjusteSencilloAnte(Long ajusteSencilloAnte) {
        this.ajusteSencilloAnte = ajusteSencilloAnte;
    }

    public Long getAjusteSencillo() {
        return ajusteSencillo;
    }

    public Boleta ajusteSencillo(Long ajusteSencillo) {
        this.ajusteSencillo = ajusteSencillo;
        return this;
    }

    public void setAjusteSencillo(Long ajusteSencillo) {
        this.ajusteSencillo = ajusteSencillo;
    }

    public Long getMontoTotal() {
        return montoTotal;
    }

    public Boleta montoTotal(Long montoTotal) {
        this.montoTotal = montoTotal;
        return this;
    }

    public void setMontoTotal(Long montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Long getSaldoAnterior() {
        return saldoAnterior;
    }

    public Boleta saldoAnterior(Long saldoAnterior) {
        this.saldoAnterior = saldoAnterior;
        return this;
    }

    public void setSaldoAnterior(Long saldoAnterior) {
        this.saldoAnterior = saldoAnterior;
    }

    public Long getTotalPagar() {
        return totalPagar;
    }

    public Boleta totalPagar(Long totalPagar) {
        this.totalPagar = totalPagar;
        return this;
    }

    public void setTotalPagar(Long totalPagar) {
        this.totalPagar = totalPagar;
    }

    public String getTipoDespacho() {
        return tipoDespacho;
    }

    public Boleta tipoDespacho(String tipoDespacho) {
        this.tipoDespacho = tipoDespacho;
        return this;
    }

    public void setTipoDespacho(String tipoDespacho) {
        this.tipoDespacho = tipoDespacho;
    }

    public String getGrupoTarifario() {
        return grupoTarifario;
    }

    public Boleta grupoTarifario(String grupoTarifario) {
        this.grupoTarifario = grupoTarifario;
        return this;
    }

    public void setGrupoTarifario(String grupoTarifario) {
        this.grupoTarifario = grupoTarifario;
    }

    public Float getFactorCobro() {
        return factorCobro;
    }

    public Boleta factorCobro(Float factorCobro) {
        this.factorCobro = factorCobro;
        return this;
    }

    public void setFactorCobro(Float factorCobro) {
        this.factorCobro = factorCobro;
    }

    public String getTipoFactura() {
        return tipoFactura;
    }

    public Boleta tipoFactura(String tipoFactura) {
        this.tipoFactura = tipoFactura;
        return this;
    }

    public void setTipoFactura(String tipoFactura) {
        this.tipoFactura = tipoFactura;
    }

    public Integer getDiametro() {
        return diametro;
    }

    public Boleta diametro(Integer diametro) {
        this.diametro = diametro;
        return this;
    }

    public void setDiametro(Integer diametro) {
        this.diametro = diametro;
    }

    public Instant getFechaVencimiento() {
        return fechaVencimiento;
    }

    public Boleta fechaVencimiento(Instant fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
        return this;
    }

    public void setFechaVencimiento(Instant fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Instant getFechaEmision() {
        return fechaEmision;
    }

    public Boleta fechaEmision(Instant fechaEmision) {
        this.fechaEmision = fechaEmision;
        return this;
    }

    public void setFechaEmision(Instant fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public Boleta createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getLastUpdDate() {
        return lastUpdDate;
    }

    public Boleta lastUpdDate(Instant lastUpdDate) {
        this.lastUpdDate = lastUpdDate;
        return this;
    }

    public void setLastUpdDate(Instant lastUpdDate) {
        this.lastUpdDate = lastUpdDate;
    }

    public Medidor getMedidor() {
        return medidor;
    }

    public Boleta medidor(Medidor medidor) {
        this.medidor = medidor;
        return this;
    }

    public void setMedidor(Medidor medidor) {
        this.medidor = medidor;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Boleta)) {
            return false;
        }
        return id != null && id.equals(((Boleta) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Boleta{" +
            "id=" + getId() +
            ", numeroBoleta=" + getNumeroBoleta() +
            ", consumoCalculado=" + getConsumoCalculado() +
            ", consumoFacturado=" + getConsumoFacturado() +
            ", cargoFijo=" + getCargoFijo() +
            ", consumoAguaPotable=" + getConsumoAguaPotable() +
            ", servAlcantarillado=" + getServAlcantarillado() +
            ", tratAguasServidas=" + getTratAguasServidas() +
            ", subConsumoMes=" + getSubConsumoMes() +
            ", despachoPostalCer=" + getDespachoPostalCer() +
            ", intereses=" + getIntereses() +
            ", ajusteSencilloAnte=" + getAjusteSencilloAnte() +
            ", ajusteSencillo=" + getAjusteSencillo() +
            ", montoTotal=" + getMontoTotal() +
            ", saldoAnterior=" + getSaldoAnterior() +
            ", totalPagar=" + getTotalPagar() +
            ", tipoDespacho='" + getTipoDespacho() + "'" +
            ", grupoTarifario='" + getGrupoTarifario() + "'" +
            ", factorCobro=" + getFactorCobro() +
            ", tipoFactura='" + getTipoFactura() + "'" +
            ", diametro=" + getDiametro() +
            ", fechaVencimiento='" + getFechaVencimiento() + "'" +
            ", fechaEmision='" + getFechaEmision() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastUpdDate='" + getLastUpdDate() + "'" +
            "}";
    }
}
