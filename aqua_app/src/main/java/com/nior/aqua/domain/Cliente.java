package com.nior.aqua.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Cliente.
 */
@Entity
@Table(name = "cliente")
public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 20)
    @Column(name = "rut", length = 20, nullable = false, unique = true)
    private String rut;

    @NotNull
    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "otros_nombres")
    private String otrosNombres;

    @Column(name = "apellido_paterno")
    private String apellidoPaterno;

    @Column(name = "apellido_materno")
    private String apellidoMaterno;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "last_upd_date")
    private Instant lastUpdDate;

    @OneToMany(mappedBy = "cliente")
    private Set<Direccion> direccions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRut() {
        return rut;
    }

    public Cliente rut(String rut) {
        this.rut = rut;
        return this;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public Cliente nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getOtrosNombres() {
        return otrosNombres;
    }

    public Cliente otrosNombres(String otrosNombres) {
        this.otrosNombres = otrosNombres;
        return this;
    }

    public void setOtrosNombres(String otrosNombres) {
        this.otrosNombres = otrosNombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public Cliente apellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
        return this;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public Cliente apellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
        return this;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public Cliente createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getLastUpdDate() {
        return lastUpdDate;
    }

    public Cliente lastUpdDate(Instant lastUpdDate) {
        this.lastUpdDate = lastUpdDate;
        return this;
    }

    public void setLastUpdDate(Instant lastUpdDate) {
        this.lastUpdDate = lastUpdDate;
    }

    public Set<Direccion> getDireccions() {
        return direccions;
    }

    public Cliente direccions(Set<Direccion> direccions) {
        this.direccions = direccions;
        return this;
    }

    public Cliente addDireccion(Direccion direccion) {
        this.direccions.add(direccion);
        direccion.setCliente(this);
        return this;
    }

    public Cliente removeDireccion(Direccion direccion) {
        this.direccions.remove(direccion);
        direccion.setCliente(null);
        return this;
    }

    public void setDireccions(Set<Direccion> direccions) {
        this.direccions = direccions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cliente)) {
            return false;
        }
        return id != null && id.equals(((Cliente) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Cliente{" +
            "id=" + getId() +
            ", rut='" + getRut() + "'" +
            ", nombre='" + getNombre() + "'" +
            ", otrosNombres='" + getOtrosNombres() + "'" +
            ", apellidoPaterno='" + getApellidoPaterno() + "'" +
            ", apellidoMaterno='" + getApellidoMaterno() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastUpdDate='" + getLastUpdDate() + "'" +
            "}";
    }
}
