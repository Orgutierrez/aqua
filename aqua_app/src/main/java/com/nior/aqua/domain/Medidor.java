package com.nior.aqua.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Medidor.
 */
@Entity
@Table(name = "medidor")
public class Medidor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "identificador", nullable = false, unique = true)
    private String identificador;

    @NotNull
    @Column(name = "marca", nullable = false)
    private String marca;

    @Column(name = "modelo")
    private String modelo;

    @Column(name = "inicio_contador")
    private Long inicioContador;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "last_upd_date")
    private Instant lastUpdDate;

    @OneToMany(mappedBy = "medidor")
    private Set<HistoricoMedidor> historicoMedidors = new HashSet<>();

    @OneToMany(mappedBy = "medidor")
    private Set<Boleta> boletas = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("medidors")
    private Direccion direccion;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentificador() {
        return identificador;
    }

    public Medidor identificador(String identificador) {
        this.identificador = identificador;
        return this;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getMarca() {
        return marca;
    }

    public Medidor marca(String marca) {
        this.marca = marca;
        return this;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public Medidor modelo(String modelo) {
        this.modelo = modelo;
        return this;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Long getInicioContador() {
        return inicioContador;
    }

    public Medidor inicioContador(Long inicioContador) {
        this.inicioContador = inicioContador;
        return this;
    }

    public void setInicioContador(Long inicioContador) {
        this.inicioContador = inicioContador;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public Medidor createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getLastUpdDate() {
        return lastUpdDate;
    }

    public Medidor lastUpdDate(Instant lastUpdDate) {
        this.lastUpdDate = lastUpdDate;
        return this;
    }

    public void setLastUpdDate(Instant lastUpdDate) {
        this.lastUpdDate = lastUpdDate;
    }

    public Set<HistoricoMedidor> getHistoricoMedidors() {
        return historicoMedidors;
    }

    public Medidor historicoMedidors(Set<HistoricoMedidor> historicoMedidors) {
        this.historicoMedidors = historicoMedidors;
        return this;
    }

    public Medidor addHistoricoMedidor(HistoricoMedidor historicoMedidor) {
        this.historicoMedidors.add(historicoMedidor);
        historicoMedidor.setMedidor(this);
        return this;
    }

    public Medidor removeHistoricoMedidor(HistoricoMedidor historicoMedidor) {
        this.historicoMedidors.remove(historicoMedidor);
        historicoMedidor.setMedidor(null);
        return this;
    }

    public void setHistoricoMedidors(Set<HistoricoMedidor> historicoMedidors) {
        this.historicoMedidors = historicoMedidors;
    }

    public Set<Boleta> getBoletas() {
        return boletas;
    }

    public Medidor boletas(Set<Boleta> boletas) {
        this.boletas = boletas;
        return this;
    }

    public Medidor addBoleta(Boleta boleta) {
        this.boletas.add(boleta);
        boleta.setMedidor(this);
        return this;
    }

    public Medidor removeBoleta(Boleta boleta) {
        this.boletas.remove(boleta);
        boleta.setMedidor(null);
        return this;
    }

    public void setBoletas(Set<Boleta> boletas) {
        this.boletas = boletas;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public Medidor direccion(Direccion direccion) {
        this.direccion = direccion;
        return this;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Medidor)) {
            return false;
        }
        return id != null && id.equals(((Medidor) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Medidor{" +
            "id=" + getId() +
            ", identificador='" + getIdentificador() + "'" +
            ", marca='" + getMarca() + "'" +
            ", modelo='" + getModelo() + "'" +
            ", inicioContador=" + getInicioContador() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastUpdDate='" + getLastUpdDate() + "'" +
            "}";
    }
}
