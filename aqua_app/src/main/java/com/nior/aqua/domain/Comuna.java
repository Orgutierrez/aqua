package com.nior.aqua.domain;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Comuna.
 */
@Entity
@Table(name = "comuna")
public class Comuna implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom_comuna")
    private String nomComuna;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomComuna() {
        return nomComuna;
    }

    public Comuna nomComuna(String nomComuna) {
        this.nomComuna = nomComuna;
        return this;
    }

    public void setNomComuna(String nomComuna) {
        this.nomComuna = nomComuna;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Comuna)) {
            return false;
        }
        return id != null && id.equals(((Comuna) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Comuna{" +
            "id=" + getId() +
            ", nomComuna='" + getNomComuna() + "'" +
            "}";
    }
}
