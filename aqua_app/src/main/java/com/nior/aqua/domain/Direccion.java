package com.nior.aqua.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Direccion.
 */
@Entity
@Table(name = "direccion")
public class Direccion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "calle", nullable = false)
    private String calle;

    @Column(name = "numero")
    private String numero;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "last_upd_date")
    private Instant lastUpdDate;

    @OneToOne(optional = false)    @NotNull

    @JoinColumn(unique = true)
    private Pais pais;

    @OneToOne(optional = false)    @NotNull

    @JoinColumn(unique = true)
    private Region region;

    @OneToOne(optional = false)    @NotNull

    @JoinColumn(unique = true)
    private Comuna comuna;

    @OneToOne(optional = false)    @NotNull

    @JoinColumn(unique = true)
    private Ciudad ciudad;

    @OneToMany(mappedBy = "direccion")
    private Set<Medidor> medidors = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("direccions")
    private Cliente cliente;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCalle() {
        return calle;
    }

    public Direccion calle(String calle) {
        this.calle = calle;
        return this;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumero() {
        return numero;
    }

    public Direccion numero(String numero) {
        this.numero = numero;
        return this;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public Direccion createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getLastUpdDate() {
        return lastUpdDate;
    }

    public Direccion lastUpdDate(Instant lastUpdDate) {
        this.lastUpdDate = lastUpdDate;
        return this;
    }

    public void setLastUpdDate(Instant lastUpdDate) {
        this.lastUpdDate = lastUpdDate;
    }

    public Pais getPais() {
        return pais;
    }

    public Direccion pais(Pais pais) {
        this.pais = pais;
        return this;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public Region getRegion() {
        return region;
    }

    public Direccion region(Region region) {
        this.region = region;
        return this;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Comuna getComuna() {
        return comuna;
    }

    public Direccion comuna(Comuna comuna) {
        this.comuna = comuna;
        return this;
    }

    public void setComuna(Comuna comuna) {
        this.comuna = comuna;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public Direccion ciudad(Ciudad ciudad) {
        this.ciudad = ciudad;
        return this;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public Set<Medidor> getMedidors() {
        return medidors;
    }

    public Direccion medidors(Set<Medidor> medidors) {
        this.medidors = medidors;
        return this;
    }

    public Direccion addMedidor(Medidor medidor) {
        this.medidors.add(medidor);
        medidor.setDireccion(this);
        return this;
    }

    public Direccion removeMedidor(Medidor medidor) {
        this.medidors.remove(medidor);
        medidor.setDireccion(null);
        return this;
    }

    public void setMedidors(Set<Medidor> medidors) {
        this.medidors = medidors;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Direccion cliente(Cliente cliente) {
        this.cliente = cliente;
        return this;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Direccion)) {
            return false;
        }
        return id != null && id.equals(((Direccion) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Direccion{" +
            "id=" + getId() +
            ", calle='" + getCalle() + "'" +
            ", numero='" + getNumero() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastUpdDate='" + getLastUpdDate() + "'" +
            "}";
    }
}
