package com.nior.aqua.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A HistoricoMedidor.
 */
@Entity
@Table(name = "historico_medidor")
public class HistoricoMedidor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "inicio_contador", nullable = false)
    private Long inicioContador;

    @NotNull
    @Column(name = "fin_contador", nullable = false)
    private Long finContador;

    @NotNull
    @Column(name = "inicio_periodo", nullable = false)
    private Instant inicioPeriodo;

    @NotNull
    @Column(name = "fin_periodo", nullable = false)
    private Instant finPeriodo;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("historicoMedidors")
    private Medidor medidor;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInicioContador() {
        return inicioContador;
    }

    public HistoricoMedidor inicioContador(Long inicioContador) {
        this.inicioContador = inicioContador;
        return this;
    }

    public void setInicioContador(Long inicioContador) {
        this.inicioContador = inicioContador;
    }

    public Long getFinContador() {
        return finContador;
    }

    public HistoricoMedidor finContador(Long finContador) {
        this.finContador = finContador;
        return this;
    }

    public void setFinContador(Long finContador) {
        this.finContador = finContador;
    }

    public Instant getInicioPeriodo() {
        return inicioPeriodo;
    }

    public HistoricoMedidor inicioPeriodo(Instant inicioPeriodo) {
        this.inicioPeriodo = inicioPeriodo;
        return this;
    }

    public void setInicioPeriodo(Instant inicioPeriodo) {
        this.inicioPeriodo = inicioPeriodo;
    }

    public Instant getFinPeriodo() {
        return finPeriodo;
    }

    public HistoricoMedidor finPeriodo(Instant finPeriodo) {
        this.finPeriodo = finPeriodo;
        return this;
    }

    public void setFinPeriodo(Instant finPeriodo) {
        this.finPeriodo = finPeriodo;
    }

    public Medidor getMedidor() {
        return medidor;
    }

    public HistoricoMedidor medidor(Medidor medidor) {
        this.medidor = medidor;
        return this;
    }

    public void setMedidor(Medidor medidor) {
        this.medidor = medidor;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HistoricoMedidor)) {
            return false;
        }
        return id != null && id.equals(((HistoricoMedidor) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "HistoricoMedidor{" +
            "id=" + getId() +
            ", inicioContador=" + getInicioContador() +
            ", finContador=" + getFinContador() +
            ", inicioPeriodo='" + getInicioPeriodo() + "'" +
            ", finPeriodo='" + getFinPeriodo() + "'" +
            "}";
    }
}
