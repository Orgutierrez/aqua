package com.nior.aqua.domain;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Ciudad.
 */
@Entity
@Table(name = "ciudad")
public class Ciudad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom_ciudad")
    private String nomCiudad;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomCiudad() {
        return nomCiudad;
    }

    public Ciudad nomCiudad(String nomCiudad) {
        this.nomCiudad = nomCiudad;
        return this;
    }

    public void setNomCiudad(String nomCiudad) {
        this.nomCiudad = nomCiudad;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Ciudad)) {
            return false;
        }
        return id != null && id.equals(((Ciudad) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Ciudad{" +
            "id=" + getId() +
            ", nomCiudad='" + getNomCiudad() + "'" +
            "}";
    }
}
