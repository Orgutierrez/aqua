package com.nior.aqua.web.rest;

import com.nior.aqua.domain.HistoricoMedidor;
import com.nior.aqua.repository.HistoricoMedidorRepository;
import com.nior.aqua.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.nior.aqua.domain.HistoricoMedidor}.
 */
@RestController
@RequestMapping("/api")
public class HistoricoMedidorResource {

    private final Logger log = LoggerFactory.getLogger(HistoricoMedidorResource.class);

    private static final String ENTITY_NAME = "historicoMedidor";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HistoricoMedidorRepository historicoMedidorRepository;

    public HistoricoMedidorResource(HistoricoMedidorRepository historicoMedidorRepository) {
        this.historicoMedidorRepository = historicoMedidorRepository;
    }

    /**
     * {@code POST  /historico-medidors} : Create a new historicoMedidor.
     *
     * @param historicoMedidor the historicoMedidor to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new historicoMedidor, or with status {@code 400 (Bad Request)} if the historicoMedidor has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/historico-medidors")
    public ResponseEntity<HistoricoMedidor> createHistoricoMedidor(@Valid @RequestBody HistoricoMedidor historicoMedidor) throws URISyntaxException {
        log.debug("REST request to save HistoricoMedidor : {}", historicoMedidor);
        if (historicoMedidor.getId() != null) {
            throw new BadRequestAlertException("A new historicoMedidor cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HistoricoMedidor result = historicoMedidorRepository.save(historicoMedidor);
        return ResponseEntity.created(new URI("/api/historico-medidors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /historico-medidors} : Updates an existing historicoMedidor.
     *
     * @param historicoMedidor the historicoMedidor to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated historicoMedidor,
     * or with status {@code 400 (Bad Request)} if the historicoMedidor is not valid,
     * or with status {@code 500 (Internal Server Error)} if the historicoMedidor couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/historico-medidors")
    public ResponseEntity<HistoricoMedidor> updateHistoricoMedidor(@Valid @RequestBody HistoricoMedidor historicoMedidor) throws URISyntaxException {
        log.debug("REST request to update HistoricoMedidor : {}", historicoMedidor);
        if (historicoMedidor.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        HistoricoMedidor result = historicoMedidorRepository.save(historicoMedidor);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, historicoMedidor.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /historico-medidors} : get all the historicoMedidors.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of historicoMedidors in body.
     */
    @GetMapping("/historico-medidors")
    public List<HistoricoMedidor> getAllHistoricoMedidors() {
        log.debug("REST request to get all HistoricoMedidors");
        return historicoMedidorRepository.findAll();
    }

    /**
     * {@code GET  /historico-medidors/:id} : get the "id" historicoMedidor.
     *
     * @param id the id of the historicoMedidor to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the historicoMedidor, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/historico-medidors/{id}")
    public ResponseEntity<HistoricoMedidor> getHistoricoMedidor(@PathVariable Long id) {
        log.debug("REST request to get HistoricoMedidor : {}", id);
        Optional<HistoricoMedidor> historicoMedidor = historicoMedidorRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(historicoMedidor);
    }

    /**
     * {@code DELETE  /historico-medidors/:id} : delete the "id" historicoMedidor.
     *
     * @param id the id of the historicoMedidor to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/historico-medidors/{id}")
    public ResponseEntity<Void> deleteHistoricoMedidor(@PathVariable Long id) {
        log.debug("REST request to delete HistoricoMedidor : {}", id);
        historicoMedidorRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
