/**
 * View Models used by Spring MVC REST controllers.
 */
package com.nior.aqua.web.rest.vm;
