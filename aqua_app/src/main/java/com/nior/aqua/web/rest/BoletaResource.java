package com.nior.aqua.web.rest;

import com.nior.aqua.domain.Boleta;
import com.nior.aqua.repository.BoletaRepository;
import com.nior.aqua.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.nior.aqua.domain.Boleta}.
 */
@RestController
@RequestMapping("/api")
public class BoletaResource {

    private final Logger log = LoggerFactory.getLogger(BoletaResource.class);

    private static final String ENTITY_NAME = "boleta";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BoletaRepository boletaRepository;

    public BoletaResource(BoletaRepository boletaRepository) {
        this.boletaRepository = boletaRepository;
    }

    /**
     * {@code POST  /boletas} : Create a new boleta.
     *
     * @param boleta the boleta to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new boleta, or with status {@code 400 (Bad Request)} if the boleta has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/boletas")
    public ResponseEntity<Boleta> createBoleta(@Valid @RequestBody Boleta boleta) throws URISyntaxException {
        log.debug("REST request to save Boleta : {}", boleta);
        if (boleta.getId() != null) {
            throw new BadRequestAlertException("A new boleta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Boleta result = boletaRepository.save(boleta);
        return ResponseEntity.created(new URI("/api/boletas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /boletas} : Updates an existing boleta.
     *
     * @param boleta the boleta to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated boleta,
     * or with status {@code 400 (Bad Request)} if the boleta is not valid,
     * or with status {@code 500 (Internal Server Error)} if the boleta couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/boletas")
    public ResponseEntity<Boleta> updateBoleta(@Valid @RequestBody Boleta boleta) throws URISyntaxException {
        log.debug("REST request to update Boleta : {}", boleta);
        if (boleta.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Boleta result = boletaRepository.save(boleta);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, boleta.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /boletas} : get all the boletas.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of boletas in body.
     */
    @GetMapping("/boletas")
    public List<Boleta> getAllBoletas() {
        log.debug("REST request to get all Boletas");
        return boletaRepository.findAll();
    }

    /**
     * {@code GET  /boletas/:id} : get the "id" boleta.
     *
     * @param id the id of the boleta to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the boleta, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/boletas/{id}")
    public ResponseEntity<Boleta> getBoleta(@PathVariable Long id) {
        log.debug("REST request to get Boleta : {}", id);
        Optional<Boleta> boleta = boletaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(boleta);
    }

    /**
     * {@code DELETE  /boletas/:id} : delete the "id" boleta.
     *
     * @param id the id of the boleta to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/boletas/{id}")
    public ResponseEntity<Void> deleteBoleta(@PathVariable Long id) {
        log.debug("REST request to delete Boleta : {}", id);
        boletaRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
