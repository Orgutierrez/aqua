package com.nior.aqua.web.rest;

import com.nior.aqua.domain.Medidor;
import com.nior.aqua.repository.MedidorRepository;
import com.nior.aqua.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.nior.aqua.domain.Medidor}.
 */
@RestController
@RequestMapping("/api")
public class MedidorResource {

    private final Logger log = LoggerFactory.getLogger(MedidorResource.class);

    private static final String ENTITY_NAME = "medidor";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MedidorRepository medidorRepository;

    public MedidorResource(MedidorRepository medidorRepository) {
        this.medidorRepository = medidorRepository;
    }

    /**
     * {@code POST  /medidors} : Create a new medidor.
     *
     * @param medidor the medidor to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new medidor, or with status {@code 400 (Bad Request)} if the medidor has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/medidors")
    public ResponseEntity<Medidor> createMedidor(@Valid @RequestBody Medidor medidor) throws URISyntaxException {
        log.debug("REST request to save Medidor : {}", medidor);
        if (medidor.getId() != null) {
            throw new BadRequestAlertException("A new medidor cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Medidor result = medidorRepository.save(medidor);
        return ResponseEntity.created(new URI("/api/medidors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /medidors} : Updates an existing medidor.
     *
     * @param medidor the medidor to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated medidor,
     * or with status {@code 400 (Bad Request)} if the medidor is not valid,
     * or with status {@code 500 (Internal Server Error)} if the medidor couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/medidors")
    public ResponseEntity<Medidor> updateMedidor(@Valid @RequestBody Medidor medidor) throws URISyntaxException {
        log.debug("REST request to update Medidor : {}", medidor);
        if (medidor.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Medidor result = medidorRepository.save(medidor);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, medidor.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /medidors} : get all the medidors.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of medidors in body.
     */
    @GetMapping("/medidors")
    public List<Medidor> getAllMedidors() {
        log.debug("REST request to get all Medidors");
        return medidorRepository.findAll();
    }

    /**
     * {@code GET  /medidors/:id} : get the "id" medidor.
     *
     * @param id the id of the medidor to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the medidor, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/medidors/{id}")
    public ResponseEntity<Medidor> getMedidor(@PathVariable Long id) {
        log.debug("REST request to get Medidor : {}", id);
        Optional<Medidor> medidor = medidorRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(medidor);
    }

    /**
     * {@code DELETE  /medidors/:id} : delete the "id" medidor.
     *
     * @param id the id of the medidor to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/medidors/{id}")
    public ResponseEntity<Void> deleteMedidor(@PathVariable Long id) {
        log.debug("REST request to delete Medidor : {}", id);
        medidorRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
