export interface IComuna {
  id?: number;
  nomComuna?: string;
}

export class Comuna implements IComuna {
  constructor(public id?: number, public nomComuna?: string) {}
}
