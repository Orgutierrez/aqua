import { Moment } from 'moment';
import { IMedidor } from 'app/shared/model/medidor.model';

export interface IBoleta {
  id?: number;
  numeroBoleta?: number;
  consumoCalculado?: number;
  consumoFacturado?: number;
  cargoFijo?: number;
  consumoAguaPotable?: number;
  servAlcantarillado?: number;
  tratAguasServidas?: number;
  subConsumoMes?: number;
  despachoPostalCer?: number;
  intereses?: number;
  ajusteSencilloAnte?: number;
  ajusteSencillo?: number;
  montoTotal?: number;
  saldoAnterior?: number;
  totalPagar?: number;
  tipoDespacho?: string;
  grupoTarifario?: string;
  factorCobro?: number;
  tipoFactura?: string;
  diametro?: number;
  fechaVencimiento?: Moment;
  fechaEmision?: Moment;
  createdDate?: Moment;
  lastUpdDate?: Moment;
  medidor?: IMedidor;
}

export class Boleta implements IBoleta {
  constructor(
    public id?: number,
    public numeroBoleta?: number,
    public consumoCalculado?: number,
    public consumoFacturado?: number,
    public cargoFijo?: number,
    public consumoAguaPotable?: number,
    public servAlcantarillado?: number,
    public tratAguasServidas?: number,
    public subConsumoMes?: number,
    public despachoPostalCer?: number,
    public intereses?: number,
    public ajusteSencilloAnte?: number,
    public ajusteSencillo?: number,
    public montoTotal?: number,
    public saldoAnterior?: number,
    public totalPagar?: number,
    public tipoDespacho?: string,
    public grupoTarifario?: string,
    public factorCobro?: number,
    public tipoFactura?: string,
    public diametro?: number,
    public fechaVencimiento?: Moment,
    public fechaEmision?: Moment,
    public createdDate?: Moment,
    public lastUpdDate?: Moment,
    public medidor?: IMedidor
  ) {}
}
