import { Moment } from 'moment';
import { IDireccion } from 'app/shared/model/direccion.model';

export interface ICliente {
  id?: number;
  rut?: string;
  nombre?: string;
  otrosNombres?: string;
  apellidoPaterno?: string;
  apellidoMaterno?: string;
  createdDate?: Moment;
  lastUpdDate?: Moment;
  direccions?: IDireccion[];
}

export class Cliente implements ICliente {
  constructor(
    public id?: number,
    public rut?: string,
    public nombre?: string,
    public otrosNombres?: string,
    public apellidoPaterno?: string,
    public apellidoMaterno?: string,
    public createdDate?: Moment,
    public lastUpdDate?: Moment,
    public direccions?: IDireccion[]
  ) {}
}
