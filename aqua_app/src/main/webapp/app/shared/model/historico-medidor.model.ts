import { Moment } from 'moment';
import { IMedidor } from 'app/shared/model/medidor.model';

export interface IHistoricoMedidor {
  id?: number;
  inicioContador?: number;
  finContador?: number;
  inicioPeriodo?: Moment;
  finPeriodo?: Moment;
  medidor?: IMedidor;
}

export class HistoricoMedidor implements IHistoricoMedidor {
  constructor(
    public id?: number,
    public inicioContador?: number,
    public finContador?: number,
    public inicioPeriodo?: Moment,
    public finPeriodo?: Moment,
    public medidor?: IMedidor
  ) {}
}
