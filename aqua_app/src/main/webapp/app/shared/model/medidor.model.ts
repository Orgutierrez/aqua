import { Moment } from 'moment';
import { IHistoricoMedidor } from 'app/shared/model/historico-medidor.model';
import { IBoleta } from 'app/shared/model/boleta.model';
import { IDireccion } from 'app/shared/model/direccion.model';

export interface IMedidor {
  id?: number;
  identificador?: string;
  marca?: string;
  modelo?: string;
  inicioContador?: number;
  createdDate?: Moment;
  lastUpdDate?: Moment;
  historicoMedidors?: IHistoricoMedidor[];
  boletas?: IBoleta[];
  direccion?: IDireccion;
}

export class Medidor implements IMedidor {
  constructor(
    public id?: number,
    public identificador?: string,
    public marca?: string,
    public modelo?: string,
    public inicioContador?: number,
    public createdDate?: Moment,
    public lastUpdDate?: Moment,
    public historicoMedidors?: IHistoricoMedidor[],
    public boletas?: IBoleta[],
    public direccion?: IDireccion
  ) {}
}
