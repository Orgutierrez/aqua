import { Moment } from 'moment';
import { IPais } from 'app/shared/model/pais.model';
import { IRegion } from 'app/shared/model/region.model';
import { IComuna } from 'app/shared/model/comuna.model';
import { ICiudad } from 'app/shared/model/ciudad.model';
import { IMedidor } from 'app/shared/model/medidor.model';
import { ICliente } from 'app/shared/model/cliente.model';

export interface IDireccion {
  id?: number;
  calle?: string;
  numero?: string;
  createdDate?: Moment;
  lastUpdDate?: Moment;
  pais?: IPais;
  region?: IRegion;
  comuna?: IComuna;
  ciudad?: ICiudad;
  medidors?: IMedidor[];
  cliente?: ICliente;
}

export class Direccion implements IDireccion {
  constructor(
    public id?: number,
    public calle?: string,
    public numero?: string,
    public createdDate?: Moment,
    public lastUpdDate?: Moment,
    public pais?: IPais,
    public region?: IRegion,
    public comuna?: IComuna,
    public ciudad?: ICiudad,
    public medidors?: IMedidor[],
    public cliente?: ICliente
  ) {}
}
