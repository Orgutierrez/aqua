export interface ICiudad {
  id?: number;
  nomCiudad?: string;
}

export class Ciudad implements ICiudad {
  constructor(public id?: number, public nomCiudad?: string) {}
}
