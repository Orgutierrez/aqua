export interface IPais {
  id?: number;
  nomPais?: string;
}

export class Pais implements IPais {
  constructor(public id?: number, public nomPais?: string) {}
}
