export interface IRegion {
  id?: number;
  nomRegion?: string;
}

export class Region implements IRegion {
  constructor(public id?: number, public nomRegion?: string) {}
}
