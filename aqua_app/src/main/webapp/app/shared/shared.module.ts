import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AquaAppSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [AquaAppSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [AquaAppSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AquaAppSharedModule {
  static forRoot() {
    return {
      ngModule: AquaAppSharedModule
    };
  }
}
