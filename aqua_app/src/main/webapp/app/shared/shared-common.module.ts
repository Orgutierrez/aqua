import { NgModule } from '@angular/core';

import { AquaAppSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [AquaAppSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [AquaAppSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class AquaAppSharedCommonModule {}
