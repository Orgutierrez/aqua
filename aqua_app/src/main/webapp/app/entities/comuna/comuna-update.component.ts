import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IComuna, Comuna } from 'app/shared/model/comuna.model';
import { ComunaService } from './comuna.service';

@Component({
  selector: 'jhi-comuna-update',
  templateUrl: './comuna-update.component.html'
})
export class ComunaUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    nomComuna: []
  });

  constructor(protected comunaService: ComunaService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ comuna }) => {
      this.updateForm(comuna);
    });
  }

  updateForm(comuna: IComuna) {
    this.editForm.patchValue({
      id: comuna.id,
      nomComuna: comuna.nomComuna
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const comuna = this.createFromForm();
    if (comuna.id !== undefined) {
      this.subscribeToSaveResponse(this.comunaService.update(comuna));
    } else {
      this.subscribeToSaveResponse(this.comunaService.create(comuna));
    }
  }

  private createFromForm(): IComuna {
    return {
      ...new Comuna(),
      id: this.editForm.get(['id']).value,
      nomComuna: this.editForm.get(['nomComuna']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IComuna>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
