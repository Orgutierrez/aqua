import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IComuna } from 'app/shared/model/comuna.model';
import { AccountService } from 'app/core';
import { ComunaService } from './comuna.service';

@Component({
  selector: 'jhi-comuna',
  templateUrl: './comuna.component.html'
})
export class ComunaComponent implements OnInit, OnDestroy {
  comunas: IComuna[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected comunaService: ComunaService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.comunaService
      .query()
      .pipe(
        filter((res: HttpResponse<IComuna[]>) => res.ok),
        map((res: HttpResponse<IComuna[]>) => res.body)
      )
      .subscribe(
        (res: IComuna[]) => {
          this.comunas = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInComunas();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IComuna) {
    return item.id;
  }

  registerChangeInComunas() {
    this.eventSubscriber = this.eventManager.subscribe('comunaListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
