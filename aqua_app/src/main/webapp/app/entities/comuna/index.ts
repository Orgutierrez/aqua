export * from './comuna.service';
export * from './comuna-update.component';
export * from './comuna-delete-dialog.component';
export * from './comuna-detail.component';
export * from './comuna.component';
export * from './comuna.route';
