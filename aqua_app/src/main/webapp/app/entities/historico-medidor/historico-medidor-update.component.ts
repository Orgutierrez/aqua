import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IHistoricoMedidor, HistoricoMedidor } from 'app/shared/model/historico-medidor.model';
import { HistoricoMedidorService } from './historico-medidor.service';
import { IMedidor } from 'app/shared/model/medidor.model';
import { MedidorService } from 'app/entities/medidor';

@Component({
  selector: 'jhi-historico-medidor-update',
  templateUrl: './historico-medidor-update.component.html'
})
export class HistoricoMedidorUpdateComponent implements OnInit {
  isSaving: boolean;

  medidors: IMedidor[];

  editForm = this.fb.group({
    id: [],
    inicioContador: [null, [Validators.required]],
    finContador: [null, [Validators.required]],
    inicioPeriodo: [null, [Validators.required]],
    finPeriodo: [null, [Validators.required]],
    medidor: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected historicoMedidorService: HistoricoMedidorService,
    protected medidorService: MedidorService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ historicoMedidor }) => {
      this.updateForm(historicoMedidor);
    });
    this.medidorService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMedidor[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMedidor[]>) => response.body)
      )
      .subscribe((res: IMedidor[]) => (this.medidors = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(historicoMedidor: IHistoricoMedidor) {
    this.editForm.patchValue({
      id: historicoMedidor.id,
      inicioContador: historicoMedidor.inicioContador,
      finContador: historicoMedidor.finContador,
      inicioPeriodo: historicoMedidor.inicioPeriodo != null ? historicoMedidor.inicioPeriodo.format(DATE_TIME_FORMAT) : null,
      finPeriodo: historicoMedidor.finPeriodo != null ? historicoMedidor.finPeriodo.format(DATE_TIME_FORMAT) : null,
      medidor: historicoMedidor.medidor
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const historicoMedidor = this.createFromForm();
    if (historicoMedidor.id !== undefined) {
      this.subscribeToSaveResponse(this.historicoMedidorService.update(historicoMedidor));
    } else {
      this.subscribeToSaveResponse(this.historicoMedidorService.create(historicoMedidor));
    }
  }

  private createFromForm(): IHistoricoMedidor {
    return {
      ...new HistoricoMedidor(),
      id: this.editForm.get(['id']).value,
      inicioContador: this.editForm.get(['inicioContador']).value,
      finContador: this.editForm.get(['finContador']).value,
      inicioPeriodo:
        this.editForm.get(['inicioPeriodo']).value != null
          ? moment(this.editForm.get(['inicioPeriodo']).value, DATE_TIME_FORMAT)
          : undefined,
      finPeriodo:
        this.editForm.get(['finPeriodo']).value != null ? moment(this.editForm.get(['finPeriodo']).value, DATE_TIME_FORMAT) : undefined,
      medidor: this.editForm.get(['medidor']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHistoricoMedidor>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackMedidorById(index: number, item: IMedidor) {
    return item.id;
  }
}
