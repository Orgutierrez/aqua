import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IHistoricoMedidor } from 'app/shared/model/historico-medidor.model';
import { AccountService } from 'app/core';
import { HistoricoMedidorService } from './historico-medidor.service';

@Component({
  selector: 'jhi-historico-medidor',
  templateUrl: './historico-medidor.component.html'
})
export class HistoricoMedidorComponent implements OnInit, OnDestroy {
  historicoMedidors: IHistoricoMedidor[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected historicoMedidorService: HistoricoMedidorService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.historicoMedidorService
      .query()
      .pipe(
        filter((res: HttpResponse<IHistoricoMedidor[]>) => res.ok),
        map((res: HttpResponse<IHistoricoMedidor[]>) => res.body)
      )
      .subscribe(
        (res: IHistoricoMedidor[]) => {
          this.historicoMedidors = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInHistoricoMedidors();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IHistoricoMedidor) {
    return item.id;
  }

  registerChangeInHistoricoMedidors() {
    this.eventSubscriber = this.eventManager.subscribe('historicoMedidorListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
