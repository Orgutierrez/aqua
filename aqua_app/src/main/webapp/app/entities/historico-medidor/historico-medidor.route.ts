import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { HistoricoMedidor } from 'app/shared/model/historico-medidor.model';
import { HistoricoMedidorService } from './historico-medidor.service';
import { HistoricoMedidorComponent } from './historico-medidor.component';
import { HistoricoMedidorDetailComponent } from './historico-medidor-detail.component';
import { HistoricoMedidorUpdateComponent } from './historico-medidor-update.component';
import { HistoricoMedidorDeletePopupComponent } from './historico-medidor-delete-dialog.component';
import { IHistoricoMedidor } from 'app/shared/model/historico-medidor.model';

@Injectable({ providedIn: 'root' })
export class HistoricoMedidorResolve implements Resolve<IHistoricoMedidor> {
  constructor(private service: HistoricoMedidorService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IHistoricoMedidor> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<HistoricoMedidor>) => response.ok),
        map((historicoMedidor: HttpResponse<HistoricoMedidor>) => historicoMedidor.body)
      );
    }
    return of(new HistoricoMedidor());
  }
}

export const historicoMedidorRoute: Routes = [
  {
    path: '',
    component: HistoricoMedidorComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'HistoricoMedidors'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: HistoricoMedidorDetailComponent,
    resolve: {
      historicoMedidor: HistoricoMedidorResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'HistoricoMedidors'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: HistoricoMedidorUpdateComponent,
    resolve: {
      historicoMedidor: HistoricoMedidorResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'HistoricoMedidors'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: HistoricoMedidorUpdateComponent,
    resolve: {
      historicoMedidor: HistoricoMedidorResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'HistoricoMedidors'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const historicoMedidorPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: HistoricoMedidorDeletePopupComponent,
    resolve: {
      historicoMedidor: HistoricoMedidorResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'HistoricoMedidors'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
