import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AquaAppSharedModule } from 'app/shared';
import {
  HistoricoMedidorComponent,
  HistoricoMedidorDetailComponent,
  HistoricoMedidorUpdateComponent,
  HistoricoMedidorDeletePopupComponent,
  HistoricoMedidorDeleteDialogComponent,
  historicoMedidorRoute,
  historicoMedidorPopupRoute
} from './';

const ENTITY_STATES = [...historicoMedidorRoute, ...historicoMedidorPopupRoute];

@NgModule({
  imports: [AquaAppSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    HistoricoMedidorComponent,
    HistoricoMedidorDetailComponent,
    HistoricoMedidorUpdateComponent,
    HistoricoMedidorDeleteDialogComponent,
    HistoricoMedidorDeletePopupComponent
  ],
  entryComponents: [
    HistoricoMedidorComponent,
    HistoricoMedidorUpdateComponent,
    HistoricoMedidorDeleteDialogComponent,
    HistoricoMedidorDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AquaAppHistoricoMedidorModule {}
