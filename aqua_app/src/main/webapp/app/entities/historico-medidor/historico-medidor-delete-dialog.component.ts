import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IHistoricoMedidor } from 'app/shared/model/historico-medidor.model';
import { HistoricoMedidorService } from './historico-medidor.service';

@Component({
  selector: 'jhi-historico-medidor-delete-dialog',
  templateUrl: './historico-medidor-delete-dialog.component.html'
})
export class HistoricoMedidorDeleteDialogComponent {
  historicoMedidor: IHistoricoMedidor;

  constructor(
    protected historicoMedidorService: HistoricoMedidorService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.historicoMedidorService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'historicoMedidorListModification',
        content: 'Deleted an historicoMedidor'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-historico-medidor-delete-popup',
  template: ''
})
export class HistoricoMedidorDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ historicoMedidor }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(HistoricoMedidorDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.historicoMedidor = historicoMedidor;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/historico-medidor', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/historico-medidor', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
