import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IHistoricoMedidor } from 'app/shared/model/historico-medidor.model';

@Component({
  selector: 'jhi-historico-medidor-detail',
  templateUrl: './historico-medidor-detail.component.html'
})
export class HistoricoMedidorDetailComponent implements OnInit {
  historicoMedidor: IHistoricoMedidor;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ historicoMedidor }) => {
      this.historicoMedidor = historicoMedidor;
    });
  }

  previousState() {
    window.history.back();
  }
}
