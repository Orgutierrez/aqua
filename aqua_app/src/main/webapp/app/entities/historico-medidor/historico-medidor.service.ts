import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IHistoricoMedidor } from 'app/shared/model/historico-medidor.model';

type EntityResponseType = HttpResponse<IHistoricoMedidor>;
type EntityArrayResponseType = HttpResponse<IHistoricoMedidor[]>;

@Injectable({ providedIn: 'root' })
export class HistoricoMedidorService {
  public resourceUrl = SERVER_API_URL + 'api/historico-medidors';

  constructor(protected http: HttpClient) {}

  create(historicoMedidor: IHistoricoMedidor): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(historicoMedidor);
    return this.http
      .post<IHistoricoMedidor>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(historicoMedidor: IHistoricoMedidor): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(historicoMedidor);
    return this.http
      .put<IHistoricoMedidor>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IHistoricoMedidor>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IHistoricoMedidor[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(historicoMedidor: IHistoricoMedidor): IHistoricoMedidor {
    const copy: IHistoricoMedidor = Object.assign({}, historicoMedidor, {
      inicioPeriodo:
        historicoMedidor.inicioPeriodo != null && historicoMedidor.inicioPeriodo.isValid() ? historicoMedidor.inicioPeriodo.toJSON() : null,
      finPeriodo: historicoMedidor.finPeriodo != null && historicoMedidor.finPeriodo.isValid() ? historicoMedidor.finPeriodo.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.inicioPeriodo = res.body.inicioPeriodo != null ? moment(res.body.inicioPeriodo) : null;
      res.body.finPeriodo = res.body.finPeriodo != null ? moment(res.body.finPeriodo) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((historicoMedidor: IHistoricoMedidor) => {
        historicoMedidor.inicioPeriodo = historicoMedidor.inicioPeriodo != null ? moment(historicoMedidor.inicioPeriodo) : null;
        historicoMedidor.finPeriodo = historicoMedidor.finPeriodo != null ? moment(historicoMedidor.finPeriodo) : null;
      });
    }
    return res;
  }
}
