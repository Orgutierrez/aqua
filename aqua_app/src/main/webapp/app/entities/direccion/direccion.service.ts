import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDireccion } from 'app/shared/model/direccion.model';

type EntityResponseType = HttpResponse<IDireccion>;
type EntityArrayResponseType = HttpResponse<IDireccion[]>;

@Injectable({ providedIn: 'root' })
export class DireccionService {
  public resourceUrl = SERVER_API_URL + 'api/direccions';

  constructor(protected http: HttpClient) {}

  create(direccion: IDireccion): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(direccion);
    return this.http
      .post<IDireccion>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(direccion: IDireccion): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(direccion);
    return this.http
      .put<IDireccion>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IDireccion>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDireccion[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(direccion: IDireccion): IDireccion {
    const copy: IDireccion = Object.assign({}, direccion, {
      createdDate: direccion.createdDate != null && direccion.createdDate.isValid() ? direccion.createdDate.toJSON() : null,
      lastUpdDate: direccion.lastUpdDate != null && direccion.lastUpdDate.isValid() ? direccion.lastUpdDate.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdDate = res.body.createdDate != null ? moment(res.body.createdDate) : null;
      res.body.lastUpdDate = res.body.lastUpdDate != null ? moment(res.body.lastUpdDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((direccion: IDireccion) => {
        direccion.createdDate = direccion.createdDate != null ? moment(direccion.createdDate) : null;
        direccion.lastUpdDate = direccion.lastUpdDate != null ? moment(direccion.lastUpdDate) : null;
      });
    }
    return res;
  }
}
