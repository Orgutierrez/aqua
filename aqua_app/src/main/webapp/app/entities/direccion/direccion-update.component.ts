import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IDireccion, Direccion } from 'app/shared/model/direccion.model';
import { DireccionService } from './direccion.service';
import { IPais } from 'app/shared/model/pais.model';
import { PaisService } from 'app/entities/pais';
import { IRegion } from 'app/shared/model/region.model';
import { RegionService } from 'app/entities/region';
import { IComuna } from 'app/shared/model/comuna.model';
import { ComunaService } from 'app/entities/comuna';
import { ICiudad } from 'app/shared/model/ciudad.model';
import { CiudadService } from 'app/entities/ciudad';
import { ICliente } from 'app/shared/model/cliente.model';
import { ClienteService } from 'app/entities/cliente';

@Component({
  selector: 'jhi-direccion-update',
  templateUrl: './direccion-update.component.html'
})
export class DireccionUpdateComponent implements OnInit {
  isSaving: boolean;

  pais: IPais[];

  regions: IRegion[];

  comunas: IComuna[];

  ciudads: ICiudad[];

  clientes: ICliente[];

  editForm = this.fb.group({
    id: [],
    calle: [null, [Validators.required]],
    numero: [],
    createdDate: [],
    lastUpdDate: [],
    pais: [null, Validators.required],
    region: [null, Validators.required],
    comuna: [null, Validators.required],
    ciudad: [null, Validators.required],
    cliente: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected direccionService: DireccionService,
    protected paisService: PaisService,
    protected regionService: RegionService,
    protected comunaService: ComunaService,
    protected ciudadService: CiudadService,
    protected clienteService: ClienteService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ direccion }) => {
      this.updateForm(direccion);
    });
    this.paisService
      .query({ filter: 'direccion-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IPais[]>) => mayBeOk.ok),
        map((response: HttpResponse<IPais[]>) => response.body)
      )
      .subscribe(
        (res: IPais[]) => {
          if (!this.editForm.get('pais').value || !this.editForm.get('pais').value.id) {
            this.pais = res;
          } else {
            this.paisService
              .find(this.editForm.get('pais').value.id)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IPais>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IPais>) => subResponse.body)
              )
              .subscribe(
                (subRes: IPais) => (this.pais = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.regionService
      .query({ filter: 'direccion-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IRegion[]>) => mayBeOk.ok),
        map((response: HttpResponse<IRegion[]>) => response.body)
      )
      .subscribe(
        (res: IRegion[]) => {
          if (!this.editForm.get('region').value || !this.editForm.get('region').value.id) {
            this.regions = res;
          } else {
            this.regionService
              .find(this.editForm.get('region').value.id)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IRegion>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IRegion>) => subResponse.body)
              )
              .subscribe(
                (subRes: IRegion) => (this.regions = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.comunaService
      .query({ filter: 'direccion-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IComuna[]>) => mayBeOk.ok),
        map((response: HttpResponse<IComuna[]>) => response.body)
      )
      .subscribe(
        (res: IComuna[]) => {
          if (!this.editForm.get('comuna').value || !this.editForm.get('comuna').value.id) {
            this.comunas = res;
          } else {
            this.comunaService
              .find(this.editForm.get('comuna').value.id)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IComuna>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IComuna>) => subResponse.body)
              )
              .subscribe(
                (subRes: IComuna) => (this.comunas = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.ciudadService
      .query({ filter: 'direccion-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<ICiudad[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICiudad[]>) => response.body)
      )
      .subscribe(
        (res: ICiudad[]) => {
          if (!this.editForm.get('ciudad').value || !this.editForm.get('ciudad').value.id) {
            this.ciudads = res;
          } else {
            this.ciudadService
              .find(this.editForm.get('ciudad').value.id)
              .pipe(
                filter((subResMayBeOk: HttpResponse<ICiudad>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<ICiudad>) => subResponse.body)
              )
              .subscribe(
                (subRes: ICiudad) => (this.ciudads = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.clienteService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICliente[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICliente[]>) => response.body)
      )
      .subscribe((res: ICliente[]) => (this.clientes = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(direccion: IDireccion) {
    this.editForm.patchValue({
      id: direccion.id,
      calle: direccion.calle,
      numero: direccion.numero,
      createdDate: direccion.createdDate != null ? direccion.createdDate.format(DATE_TIME_FORMAT) : null,
      lastUpdDate: direccion.lastUpdDate != null ? direccion.lastUpdDate.format(DATE_TIME_FORMAT) : null,
      pais: direccion.pais,
      region: direccion.region,
      comuna: direccion.comuna,
      ciudad: direccion.ciudad,
      cliente: direccion.cliente
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const direccion = this.createFromForm();
    if (direccion.id !== undefined) {
      this.subscribeToSaveResponse(this.direccionService.update(direccion));
    } else {
      this.subscribeToSaveResponse(this.direccionService.create(direccion));
    }
  }

  private createFromForm(): IDireccion {
    return {
      ...new Direccion(),
      id: this.editForm.get(['id']).value,
      calle: this.editForm.get(['calle']).value,
      numero: this.editForm.get(['numero']).value,
      createdDate:
        this.editForm.get(['createdDate']).value != null ? moment(this.editForm.get(['createdDate']).value, DATE_TIME_FORMAT) : undefined,
      lastUpdDate:
        this.editForm.get(['lastUpdDate']).value != null ? moment(this.editForm.get(['lastUpdDate']).value, DATE_TIME_FORMAT) : undefined,
      pais: this.editForm.get(['pais']).value,
      region: this.editForm.get(['region']).value,
      comuna: this.editForm.get(['comuna']).value,
      ciudad: this.editForm.get(['ciudad']).value,
      cliente: this.editForm.get(['cliente']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDireccion>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackPaisById(index: number, item: IPais) {
    return item.id;
  }

  trackRegionById(index: number, item: IRegion) {
    return item.id;
  }

  trackComunaById(index: number, item: IComuna) {
    return item.id;
  }

  trackCiudadById(index: number, item: ICiudad) {
    return item.id;
  }

  trackClienteById(index: number, item: ICliente) {
    return item.id;
  }
}
