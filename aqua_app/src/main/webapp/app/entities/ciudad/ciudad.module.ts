import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AquaAppSharedModule } from 'app/shared';
import {
  CiudadComponent,
  CiudadDetailComponent,
  CiudadUpdateComponent,
  CiudadDeletePopupComponent,
  CiudadDeleteDialogComponent,
  ciudadRoute,
  ciudadPopupRoute
} from './';

const ENTITY_STATES = [...ciudadRoute, ...ciudadPopupRoute];

@NgModule({
  imports: [AquaAppSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [CiudadComponent, CiudadDetailComponent, CiudadUpdateComponent, CiudadDeleteDialogComponent, CiudadDeletePopupComponent],
  entryComponents: [CiudadComponent, CiudadUpdateComponent, CiudadDeleteDialogComponent, CiudadDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AquaAppCiudadModule {}
