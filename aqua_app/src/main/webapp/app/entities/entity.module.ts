import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'cliente',
        loadChildren: () => import('./cliente/cliente.module').then(m => m.AquaAppClienteModule)
      },
      {
        path: 'direccion',
        loadChildren: () => import('./direccion/direccion.module').then(m => m.AquaAppDireccionModule)
      },
      {
        path: 'historico-medidor',
        loadChildren: () => import('./historico-medidor/historico-medidor.module').then(m => m.AquaAppHistoricoMedidorModule)
      },
      {
        path: 'medidor',
        loadChildren: () => import('./medidor/medidor.module').then(m => m.AquaAppMedidorModule)
      },
      {
        path: 'boleta',
        loadChildren: () => import('./boleta/boleta.module').then(m => m.AquaAppBoletaModule)
      },
      {
        path: 'pais',
        loadChildren: () => import('./pais/pais.module').then(m => m.AquaAppPaisModule)
      },
      {
        path: 'comuna',
        loadChildren: () => import('./comuna/comuna.module').then(m => m.AquaAppComunaModule)
      },
      {
        path: 'region',
        loadChildren: () => import('./region/region.module').then(m => m.AquaAppRegionModule)
      },
      {
        path: 'ciudad',
        loadChildren: () => import('./ciudad/ciudad.module').then(m => m.AquaAppCiudadModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AquaAppEntityModule {}
