import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMedidor } from 'app/shared/model/medidor.model';

type EntityResponseType = HttpResponse<IMedidor>;
type EntityArrayResponseType = HttpResponse<IMedidor[]>;

@Injectable({ providedIn: 'root' })
export class MedidorService {
  public resourceUrl = SERVER_API_URL + 'api/medidors';

  constructor(protected http: HttpClient) {}

  create(medidor: IMedidor): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(medidor);
    return this.http
      .post<IMedidor>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(medidor: IMedidor): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(medidor);
    return this.http
      .put<IMedidor>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMedidor>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMedidor[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(medidor: IMedidor): IMedidor {
    const copy: IMedidor = Object.assign({}, medidor, {
      createdDate: medidor.createdDate != null && medidor.createdDate.isValid() ? medidor.createdDate.toJSON() : null,
      lastUpdDate: medidor.lastUpdDate != null && medidor.lastUpdDate.isValid() ? medidor.lastUpdDate.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdDate = res.body.createdDate != null ? moment(res.body.createdDate) : null;
      res.body.lastUpdDate = res.body.lastUpdDate != null ? moment(res.body.lastUpdDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((medidor: IMedidor) => {
        medidor.createdDate = medidor.createdDate != null ? moment(medidor.createdDate) : null;
        medidor.lastUpdDate = medidor.lastUpdDate != null ? moment(medidor.lastUpdDate) : null;
      });
    }
    return res;
  }
}
