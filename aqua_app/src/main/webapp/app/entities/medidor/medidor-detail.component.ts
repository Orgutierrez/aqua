import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMedidor } from 'app/shared/model/medidor.model';

@Component({
  selector: 'jhi-medidor-detail',
  templateUrl: './medidor-detail.component.html'
})
export class MedidorDetailComponent implements OnInit {
  medidor: IMedidor;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ medidor }) => {
      this.medidor = medidor;
    });
  }

  previousState() {
    window.history.back();
  }
}
