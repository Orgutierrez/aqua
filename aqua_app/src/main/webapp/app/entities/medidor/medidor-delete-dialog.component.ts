import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMedidor } from 'app/shared/model/medidor.model';
import { MedidorService } from './medidor.service';

@Component({
  selector: 'jhi-medidor-delete-dialog',
  templateUrl: './medidor-delete-dialog.component.html'
})
export class MedidorDeleteDialogComponent {
  medidor: IMedidor;

  constructor(protected medidorService: MedidorService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.medidorService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'medidorListModification',
        content: 'Deleted an medidor'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-medidor-delete-popup',
  template: ''
})
export class MedidorDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ medidor }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(MedidorDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.medidor = medidor;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/medidor', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/medidor', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
