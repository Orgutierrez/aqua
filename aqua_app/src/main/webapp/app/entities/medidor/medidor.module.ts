import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AquaAppSharedModule } from 'app/shared';
import {
  MedidorComponent,
  MedidorDetailComponent,
  MedidorUpdateComponent,
  MedidorDeletePopupComponent,
  MedidorDeleteDialogComponent,
  medidorRoute,
  medidorPopupRoute
} from './';

const ENTITY_STATES = [...medidorRoute, ...medidorPopupRoute];

@NgModule({
  imports: [AquaAppSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    MedidorComponent,
    MedidorDetailComponent,
    MedidorUpdateComponent,
    MedidorDeleteDialogComponent,
    MedidorDeletePopupComponent
  ],
  entryComponents: [MedidorComponent, MedidorUpdateComponent, MedidorDeleteDialogComponent, MedidorDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AquaAppMedidorModule {}
