import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Medidor } from 'app/shared/model/medidor.model';
import { MedidorService } from './medidor.service';
import { MedidorComponent } from './medidor.component';
import { MedidorDetailComponent } from './medidor-detail.component';
import { MedidorUpdateComponent } from './medidor-update.component';
import { MedidorDeletePopupComponent } from './medidor-delete-dialog.component';
import { IMedidor } from 'app/shared/model/medidor.model';

@Injectable({ providedIn: 'root' })
export class MedidorResolve implements Resolve<IMedidor> {
  constructor(private service: MedidorService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMedidor> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Medidor>) => response.ok),
        map((medidor: HttpResponse<Medidor>) => medidor.body)
      );
    }
    return of(new Medidor());
  }
}

export const medidorRoute: Routes = [
  {
    path: '',
    component: MedidorComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Medidors'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MedidorDetailComponent,
    resolve: {
      medidor: MedidorResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Medidors'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MedidorUpdateComponent,
    resolve: {
      medidor: MedidorResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Medidors'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MedidorUpdateComponent,
    resolve: {
      medidor: MedidorResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Medidors'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const medidorPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MedidorDeletePopupComponent,
    resolve: {
      medidor: MedidorResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Medidors'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
