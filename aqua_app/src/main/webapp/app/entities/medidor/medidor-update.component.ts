import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IMedidor, Medidor } from 'app/shared/model/medidor.model';
import { MedidorService } from './medidor.service';
import { IDireccion } from 'app/shared/model/direccion.model';
import { DireccionService } from 'app/entities/direccion';

@Component({
  selector: 'jhi-medidor-update',
  templateUrl: './medidor-update.component.html'
})
export class MedidorUpdateComponent implements OnInit {
  isSaving: boolean;

  direccions: IDireccion[];

  editForm = this.fb.group({
    id: [],
    identificador: [null, [Validators.required]],
    marca: [null, [Validators.required]],
    modelo: [],
    inicioContador: [],
    createdDate: [],
    lastUpdDate: [],
    direccion: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected medidorService: MedidorService,
    protected direccionService: DireccionService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ medidor }) => {
      this.updateForm(medidor);
    });
    this.direccionService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IDireccion[]>) => mayBeOk.ok),
        map((response: HttpResponse<IDireccion[]>) => response.body)
      )
      .subscribe((res: IDireccion[]) => (this.direccions = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(medidor: IMedidor) {
    this.editForm.patchValue({
      id: medidor.id,
      identificador: medidor.identificador,
      marca: medidor.marca,
      modelo: medidor.modelo,
      inicioContador: medidor.inicioContador,
      createdDate: medidor.createdDate != null ? medidor.createdDate.format(DATE_TIME_FORMAT) : null,
      lastUpdDate: medidor.lastUpdDate != null ? medidor.lastUpdDate.format(DATE_TIME_FORMAT) : null,
      direccion: medidor.direccion
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const medidor = this.createFromForm();
    if (medidor.id !== undefined) {
      this.subscribeToSaveResponse(this.medidorService.update(medidor));
    } else {
      this.subscribeToSaveResponse(this.medidorService.create(medidor));
    }
  }

  private createFromForm(): IMedidor {
    return {
      ...new Medidor(),
      id: this.editForm.get(['id']).value,
      identificador: this.editForm.get(['identificador']).value,
      marca: this.editForm.get(['marca']).value,
      modelo: this.editForm.get(['modelo']).value,
      inicioContador: this.editForm.get(['inicioContador']).value,
      createdDate:
        this.editForm.get(['createdDate']).value != null ? moment(this.editForm.get(['createdDate']).value, DATE_TIME_FORMAT) : undefined,
      lastUpdDate:
        this.editForm.get(['lastUpdDate']).value != null ? moment(this.editForm.get(['lastUpdDate']).value, DATE_TIME_FORMAT) : undefined,
      direccion: this.editForm.get(['direccion']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMedidor>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackDireccionById(index: number, item: IDireccion) {
    return item.id;
  }
}
