import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBoleta } from 'app/shared/model/boleta.model';

@Component({
  selector: 'jhi-boleta-detail',
  templateUrl: './boleta-detail.component.html'
})
export class BoletaDetailComponent implements OnInit {
  boleta: IBoleta;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ boleta }) => {
      this.boleta = boleta;
    });
  }

  previousState() {
    window.history.back();
  }
}
