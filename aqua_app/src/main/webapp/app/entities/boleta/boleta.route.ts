import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Boleta } from 'app/shared/model/boleta.model';
import { BoletaService } from './boleta.service';
import { BoletaComponent } from './boleta.component';
import { BoletaDetailComponent } from './boleta-detail.component';
import { BoletaUpdateComponent } from './boleta-update.component';
import { BoletaDeletePopupComponent } from './boleta-delete-dialog.component';
import { IBoleta } from 'app/shared/model/boleta.model';

@Injectable({ providedIn: 'root' })
export class BoletaResolve implements Resolve<IBoleta> {
  constructor(private service: BoletaService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBoleta> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Boleta>) => response.ok),
        map((boleta: HttpResponse<Boleta>) => boleta.body)
      );
    }
    return of(new Boleta());
  }
}

export const boletaRoute: Routes = [
  {
    path: '',
    component: BoletaComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Boletas'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BoletaDetailComponent,
    resolve: {
      boleta: BoletaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Boletas'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BoletaUpdateComponent,
    resolve: {
      boleta: BoletaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Boletas'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BoletaUpdateComponent,
    resolve: {
      boleta: BoletaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Boletas'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const boletaPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: BoletaDeletePopupComponent,
    resolve: {
      boleta: BoletaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Boletas'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
