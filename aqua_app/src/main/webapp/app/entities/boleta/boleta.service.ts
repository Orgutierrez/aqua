import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBoleta } from 'app/shared/model/boleta.model';

type EntityResponseType = HttpResponse<IBoleta>;
type EntityArrayResponseType = HttpResponse<IBoleta[]>;

@Injectable({ providedIn: 'root' })
export class BoletaService {
  public resourceUrl = SERVER_API_URL + 'api/boletas';

  constructor(protected http: HttpClient) {}

  create(boleta: IBoleta): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(boleta);
    return this.http
      .post<IBoleta>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(boleta: IBoleta): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(boleta);
    return this.http
      .put<IBoleta>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBoleta>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBoleta[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(boleta: IBoleta): IBoleta {
    const copy: IBoleta = Object.assign({}, boleta, {
      fechaVencimiento: boleta.fechaVencimiento != null && boleta.fechaVencimiento.isValid() ? boleta.fechaVencimiento.toJSON() : null,
      fechaEmision: boleta.fechaEmision != null && boleta.fechaEmision.isValid() ? boleta.fechaEmision.toJSON() : null,
      createdDate: boleta.createdDate != null && boleta.createdDate.isValid() ? boleta.createdDate.toJSON() : null,
      lastUpdDate: boleta.lastUpdDate != null && boleta.lastUpdDate.isValid() ? boleta.lastUpdDate.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.fechaVencimiento = res.body.fechaVencimiento != null ? moment(res.body.fechaVencimiento) : null;
      res.body.fechaEmision = res.body.fechaEmision != null ? moment(res.body.fechaEmision) : null;
      res.body.createdDate = res.body.createdDate != null ? moment(res.body.createdDate) : null;
      res.body.lastUpdDate = res.body.lastUpdDate != null ? moment(res.body.lastUpdDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((boleta: IBoleta) => {
        boleta.fechaVencimiento = boleta.fechaVencimiento != null ? moment(boleta.fechaVencimiento) : null;
        boleta.fechaEmision = boleta.fechaEmision != null ? moment(boleta.fechaEmision) : null;
        boleta.createdDate = boleta.createdDate != null ? moment(boleta.createdDate) : null;
        boleta.lastUpdDate = boleta.lastUpdDate != null ? moment(boleta.lastUpdDate) : null;
      });
    }
    return res;
  }
}
