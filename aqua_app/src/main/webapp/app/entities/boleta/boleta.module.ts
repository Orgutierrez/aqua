import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AquaAppSharedModule } from 'app/shared';
import {
  BoletaComponent,
  BoletaDetailComponent,
  BoletaUpdateComponent,
  BoletaDeletePopupComponent,
  BoletaDeleteDialogComponent,
  boletaRoute,
  boletaPopupRoute
} from './';

const ENTITY_STATES = [...boletaRoute, ...boletaPopupRoute];

@NgModule({
  imports: [AquaAppSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [BoletaComponent, BoletaDetailComponent, BoletaUpdateComponent, BoletaDeleteDialogComponent, BoletaDeletePopupComponent],
  entryComponents: [BoletaComponent, BoletaUpdateComponent, BoletaDeleteDialogComponent, BoletaDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AquaAppBoletaModule {}
