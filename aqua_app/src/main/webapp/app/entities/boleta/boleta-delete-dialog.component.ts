import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBoleta } from 'app/shared/model/boleta.model';
import { BoletaService } from './boleta.service';

@Component({
  selector: 'jhi-boleta-delete-dialog',
  templateUrl: './boleta-delete-dialog.component.html'
})
export class BoletaDeleteDialogComponent {
  boleta: IBoleta;

  constructor(protected boletaService: BoletaService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.boletaService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'boletaListModification',
        content: 'Deleted an boleta'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-boleta-delete-popup',
  template: ''
})
export class BoletaDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ boleta }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(BoletaDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.boleta = boleta;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/boleta', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/boleta', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
