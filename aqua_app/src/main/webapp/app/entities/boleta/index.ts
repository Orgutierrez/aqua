export * from './boleta.service';
export * from './boleta-update.component';
export * from './boleta-delete-dialog.component';
export * from './boleta-detail.component';
export * from './boleta.component';
export * from './boleta.route';
