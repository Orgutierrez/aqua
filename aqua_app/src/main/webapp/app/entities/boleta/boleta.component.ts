import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IBoleta } from 'app/shared/model/boleta.model';
import { AccountService } from 'app/core';
import { BoletaService } from './boleta.service';

@Component({
  selector: 'jhi-boleta',
  templateUrl: './boleta.component.html'
})
export class BoletaComponent implements OnInit, OnDestroy {
  boletas: IBoleta[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected boletaService: BoletaService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.boletaService
      .query()
      .pipe(
        filter((res: HttpResponse<IBoleta[]>) => res.ok),
        map((res: HttpResponse<IBoleta[]>) => res.body)
      )
      .subscribe(
        (res: IBoleta[]) => {
          this.boletas = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInBoletas();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IBoleta) {
    return item.id;
  }

  registerChangeInBoletas() {
    this.eventSubscriber = this.eventManager.subscribe('boletaListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
