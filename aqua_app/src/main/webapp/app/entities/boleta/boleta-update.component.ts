import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IBoleta, Boleta } from 'app/shared/model/boleta.model';
import { BoletaService } from './boleta.service';
import { IMedidor } from 'app/shared/model/medidor.model';
import { MedidorService } from 'app/entities/medidor';

@Component({
  selector: 'jhi-boleta-update',
  templateUrl: './boleta-update.component.html'
})
export class BoletaUpdateComponent implements OnInit {
  isSaving: boolean;

  medidors: IMedidor[];

  editForm = this.fb.group({
    id: [],
    numeroBoleta: [null, [Validators.required]],
    consumoCalculado: [null, [Validators.required]],
    consumoFacturado: [null, [Validators.required]],
    cargoFijo: [null, [Validators.required]],
    consumoAguaPotable: [null, [Validators.required]],
    servAlcantarillado: [null, [Validators.required]],
    tratAguasServidas: [null, [Validators.required]],
    subConsumoMes: [null, [Validators.required]],
    despachoPostalCer: [null, [Validators.required]],
    intereses: [null, [Validators.required]],
    ajusteSencilloAnte: [null, [Validators.required]],
    ajusteSencillo: [null, [Validators.required]],
    montoTotal: [null, [Validators.required]],
    saldoAnterior: [null, [Validators.required]],
    totalPagar: [null, [Validators.required]],
    tipoDespacho: [],
    grupoTarifario: [],
    factorCobro: [null, [Validators.required]],
    tipoFactura: [null, [Validators.required]],
    diametro: [],
    fechaVencimiento: [null, [Validators.required]],
    fechaEmision: [null, [Validators.required]],
    createdDate: [],
    lastUpdDate: [],
    medidor: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected boletaService: BoletaService,
    protected medidorService: MedidorService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ boleta }) => {
      this.updateForm(boleta);
    });
    this.medidorService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMedidor[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMedidor[]>) => response.body)
      )
      .subscribe((res: IMedidor[]) => (this.medidors = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(boleta: IBoleta) {
    this.editForm.patchValue({
      id: boleta.id,
      numeroBoleta: boleta.numeroBoleta,
      consumoCalculado: boleta.consumoCalculado,
      consumoFacturado: boleta.consumoFacturado,
      cargoFijo: boleta.cargoFijo,
      consumoAguaPotable: boleta.consumoAguaPotable,
      servAlcantarillado: boleta.servAlcantarillado,
      tratAguasServidas: boleta.tratAguasServidas,
      subConsumoMes: boleta.subConsumoMes,
      despachoPostalCer: boleta.despachoPostalCer,
      intereses: boleta.intereses,
      ajusteSencilloAnte: boleta.ajusteSencilloAnte,
      ajusteSencillo: boleta.ajusteSencillo,
      montoTotal: boleta.montoTotal,
      saldoAnterior: boleta.saldoAnterior,
      totalPagar: boleta.totalPagar,
      tipoDespacho: boleta.tipoDespacho,
      grupoTarifario: boleta.grupoTarifario,
      factorCobro: boleta.factorCobro,
      tipoFactura: boleta.tipoFactura,
      diametro: boleta.diametro,
      fechaVencimiento: boleta.fechaVencimiento != null ? boleta.fechaVencimiento.format(DATE_TIME_FORMAT) : null,
      fechaEmision: boleta.fechaEmision != null ? boleta.fechaEmision.format(DATE_TIME_FORMAT) : null,
      createdDate: boleta.createdDate != null ? boleta.createdDate.format(DATE_TIME_FORMAT) : null,
      lastUpdDate: boleta.lastUpdDate != null ? boleta.lastUpdDate.format(DATE_TIME_FORMAT) : null,
      medidor: boleta.medidor
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const boleta = this.createFromForm();
    if (boleta.id !== undefined) {
      this.subscribeToSaveResponse(this.boletaService.update(boleta));
    } else {
      this.subscribeToSaveResponse(this.boletaService.create(boleta));
    }
  }

  private createFromForm(): IBoleta {
    return {
      ...new Boleta(),
      id: this.editForm.get(['id']).value,
      numeroBoleta: this.editForm.get(['numeroBoleta']).value,
      consumoCalculado: this.editForm.get(['consumoCalculado']).value,
      consumoFacturado: this.editForm.get(['consumoFacturado']).value,
      cargoFijo: this.editForm.get(['cargoFijo']).value,
      consumoAguaPotable: this.editForm.get(['consumoAguaPotable']).value,
      servAlcantarillado: this.editForm.get(['servAlcantarillado']).value,
      tratAguasServidas: this.editForm.get(['tratAguasServidas']).value,
      subConsumoMes: this.editForm.get(['subConsumoMes']).value,
      despachoPostalCer: this.editForm.get(['despachoPostalCer']).value,
      intereses: this.editForm.get(['intereses']).value,
      ajusteSencilloAnte: this.editForm.get(['ajusteSencilloAnte']).value,
      ajusteSencillo: this.editForm.get(['ajusteSencillo']).value,
      montoTotal: this.editForm.get(['montoTotal']).value,
      saldoAnterior: this.editForm.get(['saldoAnterior']).value,
      totalPagar: this.editForm.get(['totalPagar']).value,
      tipoDespacho: this.editForm.get(['tipoDespacho']).value,
      grupoTarifario: this.editForm.get(['grupoTarifario']).value,
      factorCobro: this.editForm.get(['factorCobro']).value,
      tipoFactura: this.editForm.get(['tipoFactura']).value,
      diametro: this.editForm.get(['diametro']).value,
      fechaVencimiento:
        this.editForm.get(['fechaVencimiento']).value != null
          ? moment(this.editForm.get(['fechaVencimiento']).value, DATE_TIME_FORMAT)
          : undefined,
      fechaEmision:
        this.editForm.get(['fechaEmision']).value != null ? moment(this.editForm.get(['fechaEmision']).value, DATE_TIME_FORMAT) : undefined,
      createdDate:
        this.editForm.get(['createdDate']).value != null ? moment(this.editForm.get(['createdDate']).value, DATE_TIME_FORMAT) : undefined,
      lastUpdDate:
        this.editForm.get(['lastUpdDate']).value != null ? moment(this.editForm.get(['lastUpdDate']).value, DATE_TIME_FORMAT) : undefined,
      medidor: this.editForm.get(['medidor']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBoleta>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackMedidorById(index: number, item: IMedidor) {
    return item.id;
  }
}
